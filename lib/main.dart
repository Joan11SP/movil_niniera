import 'package:flutter/material.dart';
import 'package:movil_niniera/src/Config/configSharedPreferences.dart';
import 'package:movil_niniera/src/Services/chat_socket.dart';
import 'package:movil_niniera/src/Services/provider.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/routes.dart';
import 'package:movil_niniera/src/screens/home/home_screen.dart';
import 'package:movil_niniera/src/screens/sign_in/sign_in_screen.dart';
import 'package:movil_niniera/src/theme.dart';
import 'package:provider/provider.dart';
import './src/Config/config.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => ChatService() ),
        ChangeNotifierProvider(create: (_) => ProviderInfor())
      ],
      child: MaterialApp(
        navigatorKey: navigatorKey,
        debugShowCheckedModeBanner: false,
        color: kPrimaryColor,
        title: 'Nannys',
        home: FutureBuilder(
          future: obtenerSesion(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.data != null) {
                return HomeScreen();
              } else {
                return SignInScreen();
              }
            } else {
              return Text('');
            }
          },
        ),
        theme: theme(),
        routes: routes,
      ),
    );
  }
}
