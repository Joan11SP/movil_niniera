
import 'dart:convert';

String ninioToJson(Ninio data) => json.encode(data.toJson());
class Ninio {
    String nombres;
    String idninio;
    int edad;
    String descripcion;
    String idPersona;
    Ninio({this.descripcion,this.edad,this.nombres,this.idPersona,this.idninio});

    factory Ninio.fromJson(Map<String, dynamic> json) => Ninio(
        nombres:  json['nombres'],
        edad: json["edad"],
        descripcion: json["descripcion"]
      );

  Map<String, dynamic> toJson() => {
        "nombres":nombres,
        "edad": edad,
        "descripcion": descripcion,
        'id_persona': idPersona,
        'id_ninio':idninio
  };

}