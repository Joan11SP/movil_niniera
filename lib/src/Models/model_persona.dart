import 'dart:convert';

Login loginFromJson(String str) => Login.fromJson(json.decode(str));

String loginToJson(Login data) => json.encode(data.toJson());
String personaToJson(Persona data) => json.encode(data.toJson());

class Login{
  String password;
  String email;
  String tokenFmc;

  Login({this.password,this.email,this.tokenFmc});

  factory Login.fromJson(Map<String, dynamic> json) => Login(
        email:  json['email'],
        password: json["password"],
      );

  Map<String, dynamic> toJson() => {
        "email":email,
        "password": password,
        "tokenfmc":tokenFmc
      };
}

class Persona{
  String password;
  String email;
  String firstName;
  String lastName;
  String phone;
  String dni;
  String tokenFmc;
  int type;
  Persona(
    {
      this.firstName,
      this.lastName,
      this.email,
      this.password,
      this.dni,
      this.phone,
      this.type,
      this.tokenFmc
    }
  );
  factory Persona.fromJson(Map<String, dynamic> json) => Persona(
        email:  json['email'],
        password: json["password"],
        lastName: json["last_name"],
        firstName: json["first_name"],
        phone: json["phone"],
        type: json["type"],
        dni: json["dni"],
      );

  Map<String, dynamic> toJson() => {
        "email":email,
        "password": password,
        "first_name": firstName,
        "last_name": lastName,
        "phone": phone,
        "type": 1,
        "dni": dni,
        "tokenfmc":tokenFmc
      };
}
