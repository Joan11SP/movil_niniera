import 'package:flutter/material.dart';
import 'package:movil_niniera/src/routes.dart';
import 'package:movil_niniera/src/screens/sign_in/sign_in_screen.dart';
import 'package:movil_niniera/src/theme.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Denuncias',
      theme: theme(),
      initialRoute: SignInScreen.routeName,
      routes: routes,
    );
  }
}
