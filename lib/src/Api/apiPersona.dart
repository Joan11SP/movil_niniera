import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:movil_niniera/src/Config/config.dart';
import 'package:movil_niniera/src/Config/configEncrypt.dart';
import 'package:movil_niniera/src/Models/model_ninios.dart';
import 'package:movil_niniera/src/Models/model_persona.dart';

var header = {"content-type": "application/json",'app':'app_cliente'};
var formdata = { "Content-Type":"multipart/form-data" };
var nojson = { 'app':'app_cliente' };
loginPersona(Login login){
  return http.post('$urlApi/login-persona',body: json.encode(login), headers: header);
}
nuevaPersona(Persona persona){
  return http.post('$urlApi/crear-persona',body: personaToJson(persona), headers: header);
}
pedirOtp(String email,tipo){
  return http.post('$urlApi/generar-codigo-otp',body: {'email':email,'tipo_generador_otp':tipo});
}
validarOtp(codigo, String id){
  return http.post('$urlApi/validar-codigo',body: {'codigo':codigo,'id':id});
}
recuperarClave(String passwordConfirmada, String id,String idCod){
  return http.post('$urlApi/cambiar-clave',body: {'password':passwordConfirmada,'id':id,'id_cod':idCod});
}
crearUbicacion(ubicacion){
  return http.post('$urlApi/crear-ubicacion',body: json.encode(ubicacion), headers: header);
}
actualizarUbicacion(ubicacion){
  return http.post('$urlApi/actualizar-ubicacion',body: json.encode(ubicacion), headers: header);
}
obtenerUbicaciones(persona){
  return http.post('$urlApi/buscar-ubicacion',body: {'id_persona':persona});
}
crearNinio(ninio){
  return http.post('$urlApi/crear-ninios-familia',body: ninioToJson(ninio), headers: header);
}
buscarNinios(String persona){
  return http.post('$urlApi/buscar-ninios-familia',body: {'id_persona':persona});
}
actualizarNinio(ninio){
  return http.post('$urlApi/actualizar-ninios-familia',body: ninioToJson(ninio), headers: header);
}
buscarNiniosUbicaciones(persona){
  return http.post('$urlApi/buscar-ninios-ubicacion',body: {'id_persona':persona});
}
crearSolicitud(solicitud){
  return http.post('$urlApi/crear-solicitud',body: json.encode(solicitud),headers: header);
}
//obtiene las solicitudes que envian para el servicio
buscarPropuestas(id){
  return http.post('$urlApi/buscar-propuesta',body: {'id_familia': id});
}
infoNiniera(id){
  return http.post('$urlApi/mas-info-niniera',body: {'id_persona':id});
}
crearServicioNiniera(ids){
  return http.post('$urlApi/crear-servicio-nineras',body: json.encode(ids),headers: header);
}
calificarServicio(calificar){
  return http.post('$urlApi/calificar-servicio',body: json.encode(calificar),headers: header);
}
buscarNotificacion(persona){
  return http.post('$urlApi/buscar-mis-notificaciones',body: {'id_persona':persona},headers: nojson);
}
encontrarAllNinieras(persona){
  return http.post('$urlApi/find-all-ninieras',body: {'id_familia':persona},headers: nojson);
}
detalleContrato(info){
return http.post('$urlApi/detalle-contrato',body: 
        { 
          'id_niniera': info['id_niniera'], 
          'id_familia':info['id_familia'],
          'id_solicitud':info['id_solicitud']
        },
        headers: nojson);
}

obtenerRecursoApi(url,body,header) async {
  var respuesta;
  try 
  {
    http.Response response = await http.post('$urlApi/$url',body: body,headers:header);    
    respuesta = response.body;
    
    if(respuestaEncriptada){
      respuesta = decryptAESCryptoJS(respuesta, secretKeyApi);
    }
    respuesta = json.decode(respuesta);

    return respuesta;
  } 
  on Exception catch (e) 
  {
    return respuesta = 0;
  }
  
}