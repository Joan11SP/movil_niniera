import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:movil_niniera/src/Config/config.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/size_config.dart';

class InfoBody extends StatefulWidget {
  @override
  _InfoBodyState createState() => _InfoBodyState();
}

class _InfoBodyState extends State<InfoBody> {
  int start = 2;
  var infoPersonal;
  String iconStar = 'assets/icons/star.svg';
  String iconComentario = 'assets/icons/comentario.svg';
  String iconDescripcion = 'assets/icons/descripcion.svg';
  String precio;
  var info = Icon(Icons.keyboard_arrow_down_rounded,color:kPrimaryColor);
  @override
  void initState() {
    setState(() {});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    infoPersonal = ModalRoute.of(context).settings.arguments;
    precio = infoPersonal['info_adicional']['precio'].toString().indexOf('Precio negociable') != -1? infoPersonal['info_adicional']['precio'].toString()
    : "\$${infoPersonal['info_adicional']['precio'].toString()} dòlares";
    return SafeArea(
      child: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: getProportionateScreenWidth(20),
              vertical: getProportionateScreenHeight(10)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(
                children: [
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    child: mostrarImagen(infoPersonal['photo'], context),
                    height: getProportionateScreenHeight(200),
                    width: getProportionateScreenWidth(150),
                  ),
                  SizedBox(width: getProportionateScreenWidth(15)),
                  Column(
                    children: [
                      Container(
                          padding: EdgeInsets.only(
                              left: getProportionateScreenWidth(20)),
                          width: getProportionateScreenWidth(152),
                          child: Text(infoPersonal['first_name'].toString() +
                              " " +
                              infoPersonal['last_name'].toString())),
                      SizedBox(height: getProportionateScreenHeight(15)),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          for (var i = 0; i < infoPersonal['puntuacion']; i++)
                            SvgPicture.asset(iconStar,height: 25),
                          for (var i = 0; i < 5 - infoPersonal['puntuacion']; i++)
                            SvgPicture.asset(iconStar,color: Colors.grey[400],height: 25,)
                        ],
                      ),
                    ],
                  )
                ],
              ),
              SizedBox(height: getProportionateScreenHeight(15)),
              informacion(null,Icons.calendar_today,'Se unió el',infoPersonal['create_at'],'icon'),
              SizedBox(height: getProportionateScreenHeight(15)),
              informacion(iconDescripcion,null,'Descripcion',infoPersonal['info_adicional']['descripcion'],'no_icon'),
              SizedBox(height: getProportionateScreenHeight(15)),
              informacion(null,Icons.monetization_on,'Precio por hora',precio,'icon'),
              //SizedBox(height: getProportionateScreenHeight(15)),
              informacionCollapsible('personalidad','Personalidad'),
              informacionCollapsible('habilidades','Habilidades'),
              informacionCollapsible('mas','Información adicional'),
              SizedBox(height: getProportionateScreenHeight(15)),
              Row(
                children: [
                  SvgPicture.asset(iconComentario,height: 25,color: kPrimaryColor,),
                  SizedBox(width: 15),
                  Text('Comentarios ${infoPersonal['comentarios'].length}'),
                ],
              ),
              SizedBox(height: getProportionateScreenHeight(15)),
              Container(
                child: ListView.builder(
                  shrinkWrap: true,
                  physics: const ClampingScrollPhysics(),
                  itemCount: infoPersonal['comentarios'].length,
                  itemBuilder: (_, i) {
                    return Container(
                      padding: EdgeInsets.only(
                          left: 15, bottom: getProportionateScreenHeight(15)),
                      child: Text('${i + 1}. ' +
                          " " +
                          infoPersonal['comentarios'][i]['comentario']),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  informacionCollapsible(String buscar,String titulo){
    return ExpansionTile(
      leading: Icon(Icons.info_outline,color:kPrimaryColor),
      tilePadding: EdgeInsets.only(right: 20),
      trailing:  info,
      onExpansionChanged: (value){
        if(value){
          info = Icon(Icons.keyboard_arrow_down_rounded,color:kPrimaryColor);
        }
        else {
          info = Icon(Icons.keyboard_arrow_up_rounded,color:kPrimaryColor);
        }
        setState(() {});
      },
      title: Text(titulo,style: TextStyle(fontWeight: FontWeight.w300),),
      children: [
        ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: infoPersonal['info_adicional'][buscar].length ,
          itemBuilder: (_, i) {
            return Container(
              padding: EdgeInsets.only(left: 20),
              child: Text(
                  '${i+1}. '+ " "+ infoPersonal['info_adicional'][buscar][i].toString(),
                style: TextStyle(color: kTextColor,fontSize: 15,),),
            );
          },
        ),
      ],
    );
  }

  informacion(icono,noicono,String sobre,String info,tipo){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            tipo == 'no_icon' ?
              SvgPicture.asset(icono,height: 25,color: kPrimaryColor)
              : Icon(noicono,color: kPrimaryColor,),
            SizedBox(width: 15),
            Text(sobre)
          ],
        ),
        SizedBox(height: getProportionateScreenHeight(15)),
        Container(
          padding: EdgeInsets.only(left: 25),
          child:
              Text(
                info == null ? '' :info.toString()
              ),
        ),
      ],
    );
  }
}
