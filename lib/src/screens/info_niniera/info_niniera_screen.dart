import 'package:flutter/material.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/screens/info_niniera/info_niniera.body.dart';
import 'package:movil_niniera/src/size_config.dart';

class InfoNinieraScreen extends StatelessWidget {
  static String routeName = '/info_niniera';
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 15.0,
        centerTitle: true,
        backgroundColor: kPrimaryColor,        
        title: Text('Detalle de la Niñera',style: stiloText(),),
      ),
      body: InfoBody(),
    );
  }
}