import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/screens/MapBox/mapbox_view.dart';
import 'package:movil_niniera/src/screens/Ubicaciones/ubicaciones_controller.dart';
import 'package:movil_niniera/src/size_config.dart';

class UbicaionBody extends StatefulWidget {
  @override
  _UbicaionBodyState createState() => _UbicaionBodyState();
}

class _UbicaionBodyState extends State<UbicaionBody> {
  var ubicaciones;

  getUbicacion() async {
    await Future.delayed(Duration(milliseconds: 2));
    mostrarLoading(context,'Cargando...');
    ubicaciones = await controllerGetUbicaciones(context);
    setState(() {
    });
  }

  @override
  void initState() {
    getUbicacion();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        SizedBox(height: 20.0),
        GestureDetector(
          onTap: () => Navigator.pushNamed(context, MapaScreen.routeName),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Agregar una ubicación',
                  style: TextStyle(color: kPrimaryColor, fontSize: 23.0)),
              Padding(padding: const EdgeInsets.only(right: 5.0)),
              Icon(Icons.location_on, color: kPrimaryColor, size: 30.0),
            ],
          ),
        ),
        SizedBox(height: 20.0),
        ubicaciones!= null 
        ? Container(
          height: getProportionateScreenHeight(600),
          child: ListView.builder(
            shrinkWrap: true,
            itemCount: ubicaciones.length == null ? 0 : ubicaciones.length,
            itemBuilder: (context, i) {
              return GestureDetector(
                child: Card(
                  elevation: 2.0,
                  child: ListTile(
                    title: Text(ubicaciones[i]['ubi_calles'].toString()),
                    subtitle: Text(ubicaciones[i]['ubi_referencia'].toString()),
                    isThreeLine: true,
                    trailing:
                        Icon(Icons.favorite, color: kPrimaryColor, size: 28),
                  ),
                ),
                onTap: () {
                  Navigator.pushNamed(context, MapaScreen.routeName,arguments:ubicaciones[i]);
                  mostrarMensaje(
                      ubicaciones[i]['ubi_latitud'].toString(), context, 3);
                },
              );
            },
          ),
        )
        :Text('')
      ],
    );
  }
}
