import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/screens/Ubicaciones/ubicaciones_body.dart';
import 'package:movil_niniera/src/size_config.dart';

class Ubicaciones extends StatelessWidget {
  static String routeName = '/ubicaciones';
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 15.0,
        centerTitle: true,
        backgroundColor: kPrimaryColor,
        title: Text('Selecciona una ubicacion',style: stiloText(),),
        actions: [],
      ),
      body: UbicaionBody(),
    );
  }
}