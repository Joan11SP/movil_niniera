import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:movil_niniera/src/Api/apiPersona.dart';
import 'package:movil_niniera/src/Config/config.dart';
import 'package:movil_niniera/src/Config/configEncrypt.dart';
import 'package:movil_niniera/src/Config/configSharedPreferences.dart';
import 'package:movil_niniera/src/components/botones.dart';

controllerGetUbicaciones(context) async {
  try {
    var persona = await obtenerSesion();
    var datos = json.decode(persona);
    http.Response response = await obtenerUbicaciones(datos['persona']['identificador']);    
    var respuesta;
    esconderLoading(context);
    if(respuestaEncriptada){
      respuesta = decryptAESCryptoJS(response.body, secretKeyApi);
    }
    respuesta = json.decode(response.body);
    var ubicaciones = respuesta['respuesta'];
    print('UBICACIONES ====> $ubicaciones');
    return ubicaciones;
  } on Exception catch (e) {
    print('ERROR => $e');    
    esconderLoading(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
  }

}