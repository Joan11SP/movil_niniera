import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:movil_niniera/src/Api/apiPersona.dart';
import 'package:movil_niniera/src/Config/config.dart';
import 'package:movil_niniera/src/Config/configEncrypt.dart';
import 'package:movil_niniera/src/Config/configSharedPreferences.dart';
import 'package:movil_niniera/src/Models/model_persona.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/screens/home/home_screen.dart';

controllerLoginPersona(Login login,context) async {
  try {
    http.Response response = await loginPersona(login);    
    var respuesta;

    if(respuestaEncriptada){
      respuesta = decryptAESCryptoJS(response.body, secretKeyApi);
    }
    respuesta = json.decode(response.body);
  
    Navigator.pop(context);
    if(response.statusCode == 200 && respuesta['ok'] == peticionCorrecta){
      var guardado = await guardarLogin(respuesta);
      if(guardado){
        Navigator.popAndPushNamed(context,HomeScreen.routeName);
      }else{ 
        mostrarMensaje(mensajeError, context, tiempoToast);
      }
    }
    else if (respuesta['ok'] == otpLoginCode){
      var codigo = '';
      validarOtpInicio(codigo,context,respuesta);
    }
    else{
      mostrarMensaje(respuesta['mensaje'], context, tiempoToast);
    }
  } on Exception catch (e) {
    print('ERROR => $e');Navigator.pop(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
  }

}

validarOtpInicio(codigo,context,respuesta){

  ingresarCodigoOtp(context, codigo,(res) async {
        codigo = res;
        mostrarLoading(context,'Enviando...');
        if(res.length == 6){
          var body = {
            "motivo":respuesta['motivo'],
            "id_motivo":respuesta['id_motivo'],
            "id":respuesta['id'],
            "codigo":codigo.toString()
          };
          var res = await obtenerRecursoApi('validar-otp',body,nojson);
          esconderLoading(context);
          if(res['ok'] == peticionCorrecta){
            await guardarLogin(res);
            Navigator.popAndPushNamed(context,HomeScreen.routeName);
          }else{ 
            mostrarMensaje(res['mensaje'], context, tiempoToast);
          }
          
        }
      });
}