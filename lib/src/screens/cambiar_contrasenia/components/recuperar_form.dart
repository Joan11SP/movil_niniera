import 'package:flutter/material.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/components/custom_surfix_icon.dart';
import 'package:movil_niniera/src/components/default_button.dart';
import 'package:movil_niniera/src/components/form_error.dart';
import 'package:movil_niniera/src/helper/keyboard.dart';
import 'package:movil_niniera/src/screens/cambiar_contrasenia/controller_cambiar_contrasenia.dart';
import '../../../constants.dart';
import '../../../size_config.dart';

class RecuperarForm extends StatefulWidget {
  @override
  _RecuperarFormState createState() => _RecuperarFormState();
}

class _RecuperarFormState extends State<RecuperarForm> {
  final _formKey = GlobalKey<FormState>();
  final List<String> errors = [];
  String contrasenia,confirmarContrasenia;
  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildPasswordFormField(),
          SizedBox(height: getProportionateScreenHeight(40)),
          buildConfirmaPasswordFormField(),
          FormError(errors: errors),
          SizedBox(height: getProportionateScreenHeight(40)),
          DefaultButton(
            text: "Recuperar",
            press: () {
              if (_formKey.currentState.validate()) {
                _formKey.currentState.save();
                KeyboardUtil.hideKeyboard(context);
                mostrarLoading(context,'Enviando...');
                cambiarContrasenia(contrasenia,context);
              }
            },
          ),
        ],
      ),
    );
  }
TextFormField buildPasswordFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => contrasenia = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullError);
        } else if (value.length >= 8) {
          removeError(error: kShortPassError);
        }
        contrasenia = value;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPassNullError);
          return "";
        } else if (value.length < 8) {
          addError(error: kShortPassError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Contraseña",
        hintText: "Ingresa la contraseña",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Lock.svg"),
      ),
    );
  }
TextFormField buildConfirmaPasswordFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => confirmarContrasenia= newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullError);
        } else if (value.isNotEmpty && contrasenia == confirmarContrasenia) {
          removeError(error: kMatchPassError);
        }
        confirmarContrasenia = value;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPassNullError);
          return "";
        } else if ((contrasenia != value)) {
          addError(error: kMatchPassError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Contraseña",
        hintText: "Confirmar la contraseña",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Lock.svg"),
      ),
    );
  }
}
