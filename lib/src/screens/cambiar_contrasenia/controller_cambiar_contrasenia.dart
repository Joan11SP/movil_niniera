import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:movil_niniera/src/Api/apiPersona.dart';
import 'package:movil_niniera/src/Config/config.dart';
import 'package:movil_niniera/src/Config/configEncrypt.dart';
import 'package:movil_niniera/src/Config/configSharedPreferences.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/screens/sign_in/sign_in_screen.dart';

cambiarContrasenia(String contrasenia,context) async {
  try {
    String id,idCod;
    var datos = await obtenerDatosCambiarContrasenia();
    var ids = json.decode(datos);
    id = ids['id_persona'];
    idCod = ids['id'];
    http.Response response = await recuperarClave(contrasenia,id,idCod);
    var respuesta, body;
    body = response.body;
    if (respuestaEncriptada) {
      respuesta = decryptAESCryptoJS(response.body, secretKeyApi);
      body = json.decode(respuesta);
    } else {
      body = json.decode(body);
    }
    esconderLoading(context);
    if (response.statusCode == 200 && body['ok'] == peticionCorrecta) {
       Navigator.popAndPushNamed(context,SignInScreen.routeName);
    }else{      
      mostrarMensaje(body['mensaje'], context, tiempoToast);
    }
  } on Exception catch (e) {
    print(e);
    esconderLoading(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
  }
}
