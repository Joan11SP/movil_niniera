import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/screens/Ninios/ninios_lista.dart';
import 'package:movil_niniera/src/size_config.dart';

class NiniosScreen extends StatelessWidget {
  static String routeName = '/ninios';
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 15.0,
        centerTitle: true,
        backgroundColor: kPrimaryColor,        
        title: Text('Agrega tus niños',style: stiloText()),
        actions: [],
      ),
      body: NiniosLista(),
    );
  }
}