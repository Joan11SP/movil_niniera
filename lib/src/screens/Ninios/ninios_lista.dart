import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/screens/Ninios/ninios_body.dart';
import 'package:movil_niniera/src/screens/Ninios/ninios_controller.dart';
import 'package:movil_niniera/src/size_config.dart';

class NiniosLista extends StatefulWidget {
  @override
  _NiniosListaState createState() => _NiniosListaState();
}

class _NiniosListaState extends State<NiniosLista> {
  var ninios = [];

  getNinios() async {
    await Future.delayed(Duration(milliseconds: 2));
    mostrarLoading(context,'Cargando...');
    ninios = await controllerBuscarNinios(context);
    setState(() {
    });
  }

  @override
  void initState() {
    getNinios();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        SizedBox(height: 20.0),
        GestureDetector(
          onTap: () => Navigator.pushNamed(context, NiniosForm.routeName),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Agregar sus niños',
                  style: TextStyle(color: kPrimaryColor, fontSize: 23.0)),
              Padding(padding: const EdgeInsets.only(right: 5.0)),
              Icon(Icons.person_add,color: kPrimaryColor, size: 30.0),
            ],
          ),
        ),
        SizedBox(height: 20.0),
        ninios.length>0
        ? Container(
          height: getProportionateScreenHeight(600),
          child: ListView.builder(
            shrinkWrap: true,
            itemCount: ninios.length,
            itemBuilder: (context, i) {
              return GestureDetector(
                child: Card(
                  elevation: 2.0,
                  child: ListTile(
                    title: Text(ninios[i]['nombres'].toString()),
                    subtitle: Text(ninios[i]['descripcion'].toString()),
                    isThreeLine: true
                  ),
                ),
                onTap: () {
                  Navigator.pushNamed(context, NiniosForm.routeName,arguments:ninios[i]);
                  mostrarMensaje(
                      ninios[i]['nombres'].toString(), context, 3);
                },
              );
            },
          ),
        )
        : Text('')
      ],
    );
  }
}
