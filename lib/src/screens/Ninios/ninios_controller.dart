import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:movil_niniera/src/Api/apiPersona.dart';
import 'package:movil_niniera/src/Config/config.dart';
import 'package:movil_niniera/src/Config/configEncrypt.dart';
import 'package:movil_niniera/src/Config/configSharedPreferences.dart';
import 'package:movil_niniera/src/Models/model_ninios.dart';
import 'package:movil_niniera/src/components/botones.dart';

controllerNinios(Ninio ninio, context) async {
  try {
    http.Response response; 
    if(ninio.idninio != null){
      response= await actualizarNinio(ninio);
    }else{
      response= await crearNinio(ninio);
    }
    var respuesta, body;
    body = response.body;
    if (respuestaEncriptada) {
      body = decryptAESCryptoJS(body, secretKeyApi);
    }
    respuesta = json.decode(body);
    Navigator.pop(context);
    if (respuesta['ok'] == peticionCorrecta && response.statusCode == 200) {
      mostrarMensaje(respuesta['mensaje'], context, 3);
      return true;
    } else {
      mostrarMensaje(respuesta['mensaje'], context, 3);
      return false;
    }
  } on Exception catch (e) {
    print('ERROR => $e');
    Navigator.pop(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
  }
}

crearNinios(GlobalKey<FormState> _formKey, _ninio, context) async {
  try {
    var persona = await obtenerSesion();
    var datos = json.decode(persona);
    _ninio.idPersona = datos['persona']['identificador'];
    mostrarLoading(context,'Enviando...');
    bool respuesta = await controllerNinios(_ninio, context);
    print(respuesta);
    if (respuesta) _formKey.currentState.reset();
  } on Exception catch (e) {
    Navigator.pop(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
  }
}

controllerBuscarNinios(context) async {
  var ninios;
  try {
    var persona = await obtenerSesion();
    var datos = json.decode(persona);
    String idpersona = datos['persona']['identificador'];
    
    http.Response response = await buscarNinios(idpersona);
    var respuesta, body;
    body = response.body;
    esconderLoading(context);
    if (respuestaEncriptada) {
      body = decryptAESCryptoJS(body, secretKeyApi);
    }
    respuesta = json.decode(body);
    if (respuesta['ok'] == peticionCorrecta && response.statusCode == 200) {
      return ninios = respuesta['respuesta'];
    } else {
      return ninios=[];
    }
  } on Exception catch (_) {
    esconderLoading(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
    return ninios=[];
  }
}