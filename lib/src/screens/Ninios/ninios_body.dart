import 'package:flutter/material.dart';
import 'package:movil_niniera/src/Models/model_ninios.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/components/custom_surfix_icon.dart';
import 'package:movil_niniera/src/components/default_button.dart';
import 'package:movil_niniera/src/components/form_error.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/helper/keyboard.dart';
import 'package:movil_niniera/src/screens/Ninios/ninios_controller.dart';
import 'package:movil_niniera/src/size_config.dart';

class NiniosForm extends StatefulWidget {
  static String routeName = '/ninios_form';
  @override
  _NiniosFormState createState() => _NiniosFormState();
}

class _NiniosFormState extends State<NiniosForm> {
  final _formKey = GlobalKey<FormState>();
  final List<String> errors = [];
  var ninioGuardado;
  var _ninio = new Ninio();

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    ninioGuardado = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        elevation: 15.0,
        centerTitle: true,
        backgroundColor: kPrimaryColor,
        title: Text('Añade la información', style: stiloText(),)
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: getProportionateScreenWidth(30),
            vertical: getProportionateScreenHeight(100)),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                buildNombresFormField(),
                SizedBox(height: getProportionateScreenHeight(30)),
                buildEdadFormField(),
                SizedBox(height: getProportionateScreenHeight(30)),
                buildDescripcionFormField(),
                SizedBox(height: getProportionateScreenHeight(30)),
                FormError(errors: errors),
                SizedBox(height: getProportionateScreenHeight(20)),
                DefaultButton(
                  text: ninioGuardado == null ? "Agregar" : 'Actualizar',
                  press: () async {
                    if (_formKey.currentState.validate()) {
                      _formKey.currentState.save();
                      KeyboardUtil.hideKeyboard(context);
                      if (ninioGuardado == null) {
                        await crearNinios(_formKey, _ninio, context);
                      } else {
                        _ninio.idninio = ninioGuardado['_id'];
                        await crearNinios(_formKey, _ninio, context);
                      }
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  TextFormField buildEdadFormField() {
    return TextFormField(
      initialValue:
          ninioGuardado == null ? '' : ninioGuardado['edad'].toString(),
      keyboardType: TextInputType.number,
      onSaved: (newValue) => _ninio.edad = int.parse(newValue),
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPhoneNumberNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPhoneNumberNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Edad",
        hintText: "Ingrese la edad",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Phone.svg"),
      ),
    );
  }

  TextFormField buildNombresFormField() {
    return TextFormField(
      initialValue:
          ninioGuardado == null ? '' : ninioGuardado['nombres'].toString(),
      onSaved: (newValue) => _ninio.nombres = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kNamelNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kNamelNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Nombres",
        hintText: "Ingrese los nombres",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
      ),
    );
  }

  TextFormField buildDescripcionFormField() {
    return TextFormField(
      initialValue:
          ninioGuardado == null ? '' : ninioGuardado['descripcion'].toString(),
      maxLines: 3,
      onSaved: (newValue) => _ninio.descripcion = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kNamelNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kNamelNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Descripción",
        hintText: "Es inperactivo...",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
      ),
    );
  }
}
