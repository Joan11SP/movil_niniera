import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:movil_niniera/src/Config/configSharedPreferences.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/size_config.dart';
import '../../components/botones.dart';

class PerfilScreen extends StatefulWidget {
  static String routeName = '/perfil';
  @override
  _PerfilScreenState createState() => _PerfilScreenState();
}

class _PerfilScreenState extends State<PerfilScreen> {
  Map loged;
  bool created = false;
  var person;
  String data = 'ingrese su cedula';
  //traer person JAJAJ
  getPerson() async {
    var datos = await obtenerSesion();
    var per = json.decode(datos);
    person = per['persona'];
    print(person);
    setState(() {});
  }

  body() {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[informationPerson()],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
  }

  AppBar appBar(BuildContext context) {
    return AppBar(
      elevation: 15.0,
        centerTitle: true,
        backgroundColor: kPrimaryColor,
      title: Row(
        children:[ 
          mostrarImagen(person['foto'],context),
          SizedBox(width: getProportionateScreenWidth(30),),
          Center(child: Text(person['first_name'] + " " + person['last_name'],style: stiloText(),))
        ]
      ),
    );
  }

  @override
  build(BuildContext context) {
    getPerson();
    SizeConfig().init(context);
    return Scaffold(appBar: appBar(context), body: body());
  }

  informationPerson() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 16.0),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: getProportionateScreenHeight(30),),
            TextFormField(
              enabled: false,
              decoration: textDecoration('Nombres'),
              initialValue: person['first_name'],
            ),
            SizedBox(height: getProportionateScreenHeight(30),),
            TextFormField(
              enabled: false,
              decoration: textDecoration('Apellidos'),
              initialValue: person['last_name'],
            ),
            SizedBox(height: getProportionateScreenHeight(30),),
            TextFormField(
              enabled: false,
              decoration: textDecoration('Correo'),
              initialValue: person['email'],
            ),
          ],
        ),
      ),
    );
  }

 

  InputDecoration textDecoration(String name) {
    return InputDecoration(labelText: name);
  }

  final _formKey = GlobalKey<FormState>();
}