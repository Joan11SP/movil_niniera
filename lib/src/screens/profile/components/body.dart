import 'package:flutter/material.dart';
import 'package:movil_niniera/src/Config/configSharedPreferences.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/screens/profile/notificacion.dart';
import 'package:movil_niniera/src/screens/profile/perfil.dart';
import 'package:movil_niniera/src/screens/sign_in/sign_in_screen.dart';

import 'profile_menu.dart';
import 'profile_pic.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(vertical: 20),
      child: Column(
        children: [
          ProfilePic(),
          SizedBox(height: 20),
          ProfileMenu(
            text: "Mi perfil",
            icon: "assets/icons/User Icon.svg",
            press: () => Navigator.of(context).pushNamed(PerfilScreen.routeName),
          ),
          ProfileMenu(
            text: "Notificaciones",
            icon: "assets/icons/Bell.svg",
            press: () => Navigator.of(context).pushNamed(NotificacionScreen.routeName),
          ),
          ProfileMenu(
            text: "Configuración",
            icon: "assets/icons/Settings.svg",
            press: () {},
          ),
          ProfileMenu(
            text: "Ayuda",
            icon: "assets/icons/Question mark.svg",
            press: () {},
          ),
          ProfileMenu(
            text: "Cerrar Sesión",
            icon: "assets/icons/Log out.svg",
            press: () async{
              mostrarLoading(context,'Cerrando Sesión');
              await removeSesion();
              Navigator.pop(context);
              await Navigator.of(context).pushNamedAndRemoveUntil(
                      SignInScreen.routeName, (Route<dynamic> route) => false);
            },
          ),
        ],
      ),
    );
  }
}
