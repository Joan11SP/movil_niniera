import 'dart:io';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:movil_niniera/src/Config/config.dart';
import 'package:movil_niniera/src/Config/configSharedPreferences.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/screens/profile/profile_controller.dart';
import 'package:movil_niniera/src/size_config.dart';
import 'dart:convert';
import 'dart:ui';
import 'package:path/path.dart' as path;
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class ProfilePic extends StatefulWidget {
  const ProfilePic({
    Key key,
  }) : super(key: key);

  @override
  _ProfilePicState createState() => _ProfilePicState();
}

class _ProfilePicState extends State<ProfilePic> {
  Map perfil;
  File imagen;
  Map enviarimagen = {};
  bool obtenerDatos = false;
  final picker = ImagePicker();
  selectImageGallery() async {
    try {
      imagen = null;
      mostrarLoading(context, 'Cargando...');
      final img = await picker.getImage(source: ImageSource.gallery);
      setState(() {
        imagen = File(img.path);
        var extension = path.extension(imagen.path);
        enviarimagen = {'imagen': imagen.path, 'extension': extension};
      });
      esconderLoading(context);
      if (imagen != null) {
        subirImagen();
      }
    } on NoSuchMethodError catch (e) {      
      esconderLoading(context);
    } on PlatformException catch (e) {      
      esconderLoading(context);
    }
  }
  getPerfil()async {
    var persona = await  obtenerSesion();
    perfil = json.decode(persona);
    setState(() {});
  }
  @override
  void initState() {
    imagen = null;
    getPerfil();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 115,
      width: 115,
      child: Stack(
        fit: StackFit.expand,
        overflow: Overflow.visible,
        children: [
          imagen == null && perfil == null
              ?
          CircleAvatar(
            backgroundImage: AssetImage(imagenPrueba)
          )
          : perfil != null && imagen == null?
          mostrarImagen(perfil['persona']['foto'], context)
          : CircleAvatar(
              backgroundImage:FileImage(imagen)
          ),
          Positioned(
            right: -16,
            bottom: 0,
            child: SizedBox(
              height: 46,
              width: 46,
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50),
                  side: BorderSide(color: Colors.white),
                ),
                color: Color(0xFFF5F6F9),
                onPressed: () async {
                  var status = await Permission.storage.status;
                  if (status.isGranted) {
                    selectImageGallery();
                  } else {
                    mostrarMensaje(
                        'Habilita los permisos de almacenamiento para continuar',
                        context,
                        3);
                    pedirPersmisos();
                  }
                },
                child: SvgPicture.asset("assets/icons/Camera Icon.svg"),
              ),
            ),
          )
        ],
      ),
    );
  }

  subirImagen() {
    return showDialog(
        context: context,
        child: AlertDialog(
          title: Text('Subir Imagen'),
          actions: [
            FlatButton(
              onPressed: () {
                Navigator.of(context).pop();
                setState(() {
                  imagen = null;
                });
              }, 
              child: Text('Cancelar')),
            FlatButton(
              onPressed: () {
                esconderLoading(context);
                mostrarLoading(context,'Subiendo...');
                enviarDenuncia(enviarimagen,context);
              }, 
              child: Text('Subir'))
          ],
          content: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: CircleAvatar(
              maxRadius: 35.0,
              backgroundImage: FileImage(imagen),
            ),
            height: getProportionateScreenHeight(200),
            width: getProportionateScreenWidth(150),
          ),
        ));
  }
}
