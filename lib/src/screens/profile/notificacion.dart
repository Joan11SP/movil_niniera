import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/screens/ServicioTerminado/servicio_calificar_screen.dart';
import 'package:movil_niniera/src/screens/profile/profile_controller.dart';
import 'package:movil_niniera/src/size_config.dart';

class NotificacionScreen extends StatefulWidget {
  static String routeName = '/notificaciones';
  @override
  _NotificacionScreenState createState() => _NotificacionScreenState();
}

class _NotificacionScreenState extends State<NotificacionScreen> {
  var notificaciones = [];
  String iconChat = 'assets/icons/chat.svg';
  String iconEmail = 'assets/icons/Mail.svg';
  String iconServicio ='assets/icons/solicitar.svg';

  getNotificacion() async {
    await Future.delayed(Duration(milliseconds: 2));
    mostrarLoading(context, 'Cargando...');
    var datos = await controllerGetNotificaciones(context);
    if(datos != null){
      notificaciones = datos;
    }
    setState(() {});
  }

  @override
  void initState() {
    getNotificacion();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var notificaciones2 = notificaciones;
    return Scaffold(
      appBar: AppBar(
        elevation: 15.0,
        centerTitle: true,
        backgroundColor: kPrimaryColor,
        title: Padding(
          padding: EdgeInsets.only(right: getProportionateScreenWidth(50)),
          child: Center(child: Text('Tus Notificaciones',style: stiloText(),)),
        ),
      ),
      body: bodyNotificacion(notificaciones2).length>0
      ? ListView.builder(
        shrinkWrap: true,
        itemCount: notificaciones.length,
        itemBuilder: (_, i) {
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: FlatButton(
            padding: EdgeInsets.all(20),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15)),
            color: Color(0xFFF5F6F9),
            onPressed: (){
              if(notificaciones[i]['tipo'] == 'servicio_terminado' && notificaciones[i]['status'] == 1){
                print(notificaciones[i]);
                enviarCalificar(i);
              }
            },
            child: Row(
              children: [
                SvgPicture.asset(
                  notificaciones[i]['tipo'] == 'mensaje'
                  ? iconChat
                  : notificaciones[i]['tipo'] == 'email'
                  ? iconEmail
                  : iconServicio,
                  color: kPrimaryColor,
                  width: 22,
                ),
                SizedBox(width: 20),
                Expanded(child:  Text(notificaciones[i]['informacion'].toString())),
                notificaciones[i]['tipo'] == 'servicio_terminado' && notificaciones[i]['status'] == 1 ?
                Icon(Icons.create_sharp,color:kPrimaryColor)
                : Text('')
              ],
            ),
          ),
        );
      }): Center(child:Text('No hay notificaciones')),
    );
  }

  List bodyNotificacion(List notificaciones2) => notificaciones2;
  enviarCalificar(i){
    String foto = notificaciones[i]['mas'].containsKey('foto') ? notificaciones[i]['mas']['foto'] : null;
    var arguments = {
      'id_niniera':notificaciones[i]['id_niniera'],
      'id_servicio':notificaciones[i]['mas']['id_servicio'],
      'foto':foto,'nombres':notificaciones[i]['mas']['nombres'],
      'id_notificacion':notificaciones[i]['_id']
    };
    Navigator.of(context).pushNamed(CalificarServicioScreen.routeName,
    arguments: arguments);
  }
}
