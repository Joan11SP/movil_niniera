import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:movil_niniera/src/Api/apiPersona.dart';
import 'package:movil_niniera/src/Config/config.dart';
import 'package:movil_niniera/src/Config/configEncrypt.dart';
import 'package:movil_niniera/src/Config/configSharedPreferences.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';

pedirPersmisos() async {
  Map<Permission, PermissionStatus> statuses = await [
    Permission.storage,
  ].request();
  return statuses;
}

enviarDenuncia(Map denuncia, context) async {
  try {
    var persona = await obtenerSesion();
    var datos = json.decode(persona);
    denuncia['id_persona'] = datos['persona']['identificador'];

    var req = http.MultipartRequest('POST', Uri.parse('$urlApi/crear-imagen'));
    req.fields.addAll({'id_persona': denuncia['id_persona'].toString()});
    req.files.add(await http.MultipartFile.fromPath(
      'image',
      denuncia['imagen'],
      contentType: MediaType('image', denuncia['extension']),
    ));
    http.StreamedResponse res = await req.send();
    esconderLoading(context);
    var responseData = await res.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    var respuesta = json.decode(responseString);

    if (respuesta['ok'] == peticionCorrecta && respuesta['respuesta'] != null) {
      datos['persona']['foto'] = respuesta['respuesta']['img'];
      await guardarLogin(datos);
    }
    mostrarMensaje(respuesta['mensaje'], context, 3);
  } on Exception catch (_) {
    mostrarMensaje(mensajeError, context, 2);
    esconderLoading(context);
  }
}

enviarImagenChat(chat, imagenes, context) async {
  Map enviado;
  try {
    print('CHAAAT => $chat');
    var req =
        http.MultipartRequest('POST', Uri.parse('$urlApi/crear-imagen-chat'));
    req.fields.addAll({
      'emisor': chat['emisor'],
      'receptor': chat['receptor'],
      'hour': chat['hour'],
      'date': chat['date']
    });

    req.files.add(await http.MultipartFile.fromPath(
      'image',
      imagenes['imagen'],
      contentType: MediaType('image', imagenes['extension']),
    ));

    http.StreamedResponse res = await req.send();
    esconderLoading(context);
    var responseData = await res.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    var respuesta = json.decode(responseString);

    if (respuesta['ok'] == peticionCorrecta) {
      return enviado = respuesta;
    }
    mostrarMensaje(respuesta['mensaje'], context, 3);
  } on Exception catch (_) {
    mostrarMensaje(mensajeError, context, 2);
    enviado = null;
    //esconderLoading(context);
  }
}

controllerGetNotificaciones(context) async {
  var notificacion = [];
  try {
    var datos = await obtenerSesion();
    var per = json.decode(datos);
    http.Response response =
        await buscarNotificacion(per['persona']['identificador']);
    var respuesta;

    if (respuestaEncriptada) {
      respuesta = decryptAESCryptoJS(response.body, secretKeyApi);
    }
    respuesta = json.decode(response.body);

    Navigator.pop(context);
    if (response.statusCode == 200 && respuesta['ok'] == peticionCorrecta) {
      notificacion = respuesta['respuesta'];
    } else {
      mostrarMensaje(respuesta['mensaje'], context, tiempoToast);
    }
    return notificacion;
  } on Exception catch (e) {
    Navigator.pop(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
    return notificacion;
  }
}
