import 'package:flutter/material.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/screens/ServicioTerminado/servicio_calificar.dart';
import 'package:movil_niniera/src/size_config.dart';

class CalificarServicioScreen extends StatelessWidget {
  static String routeName = "/calificar-servicio";
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 15.0,
        centerTitle: true,
        backgroundColor: kPrimaryColor,
        title: Text('Califica el servicio',style: stiloText()),
      ),
      body: CalificarServicioBody(),
    );
  }
}