import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movil_niniera/src/Config/config.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/components/default_button.dart';
import 'package:movil_niniera/src/helper/keyboard.dart';
import 'package:movil_niniera/src/screens/ServicioTerminado/servicio_calificar_controller.dart';
import 'package:movil_niniera/src/size_config.dart';

class CalificarServicioBody extends StatefulWidget {
  @override
  _CalificarServicioBodyState createState() => _CalificarServicioBodyState();
}

class _CalificarServicioBodyState extends State<CalificarServicioBody> {
  MaterialColor colorCorrupcion;
  double _upperValue = 1.0;
  var calificar;
  final TextEditingController comentario = TextEditingController();

  @override
  Widget build(BuildContext context) {
    calificar = ModalRoute.of(context).settings.arguments;
    return SafeArea(
      child: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Column(
          children: [
            SizedBox(height: getProportionateScreenHeight(30)),
            Container(
              height: getProportionateScreenHeight(150),
                      width: getProportionateScreenWidth(150),
              decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(50))),
              child: mostrarImagen(calificar['foto'], context),
            ),
            SizedBox(height: getProportionateScreenHeight(20)),
            Text(calificar['nombres']),
            SizedBox(height: getProportionateScreenHeight(40)),
            puntuacion(),
            SizedBox(height: getProportionateScreenHeight(50)),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: TextFormField(
                maxLines: 4,
                maxLength: 150,
                controller: comentario,
                decoration: InputDecoration(
                    floatingLabelBehavior: FloatingLabelBehavior.always,
                    labelText: 'Comentario'),
              ),
            ),
            SizedBox(height: getProportionateScreenHeight(30)),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: DefaultButton(
                text: 'Calificar', 
                press: obtenerDatos
              ),
            )
          ],
        ),
      ),
    );
  }

  Slider puntuacion() {
    return Slider(
      activeColor: colorCorrupcion,
      value: _upperValue,
      min: 0.0,
      max: 5.0,
      divisions: 5,
      label: 'Puntuación $_upperValue',
      onChanged: (double value) {
        setState(() {
          _upperValue = value;
          if (value <= 3.0) {
            colorCorrupcion = Colors.red;
          }
          if (value >=4.0) {
            colorCorrupcion = Colors.green;
          }
        });
      },
    );
  }

  obtenerDatos(){
    KeyboardUtil.hideKeyboard(context);
    if(comentario.text.toString().trim().length > 0){

      Map calificacion = {
        'puntuacion': _upperValue,
        'comentario':comentario.text,
        'id_niniera':calificar['id_niniera'],
        'id_servicio':calificar['id_servicio'],
        'id_notificacion':calificar['id_notificacion'],
        'id_familia':'IDDDDDDDDDDD'
      };
      mostrarLoading(context,'Cargando...');
      controllerCalificarServicio(calificacion, context);

    }else{
      mostrarMensaje(camposVacios, context, 2);
    }
  }
}
