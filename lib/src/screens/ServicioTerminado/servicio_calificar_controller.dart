import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:movil_niniera/src/Api/apiPersona.dart';
import 'package:movil_niniera/src/Config/config.dart';
import 'package:movil_niniera/src/Config/configEncrypt.dart';
import 'package:movil_niniera/src/Config/configSharedPreferences.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/screens/home/home_screen.dart';

controllerCalificarServicio(calificar, context) async {
  try {
    var persona = await obtenerSesion();
    var datos = json.decode(persona);
    calificar['id_familia'] = datos['persona']['identificador'];
    http.Response response = await calificarServicio(calificar);    
    var respuesta;
    esconderLoading(context);
    if(respuestaEncriptada){
      respuesta = decryptAESCryptoJS(response.body, secretKeyApi);
    }
    respuesta = json.decode(response.body);
    if(respuesta['ok'] == peticionCorrecta){
      Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
    }else{
    }
    print(respuesta);
    mostrarMensaje(respuesta['mensaje'], context, 3);
  } on Exception catch (e) {
    print('ERROR => $e');    
    esconderLoading(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
  }

}