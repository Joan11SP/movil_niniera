import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:movil_niniera/src/Api/apiPersona.dart';
import 'package:movil_niniera/src/Config/config.dart';
import 'package:movil_niniera/src/Config/configEncrypt.dart';
import 'package:movil_niniera/src/Config/configSharedPreferences.dart';
import 'package:movil_niniera/src/Models/model_persona.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/screens/home/home_screen.dart';
import 'package:movil_niniera/src/screens/sign_in/controller/controller_sesion.dart';

registrarPersona(Persona persona,context) async {
  try {
    http.Response response = await nuevaPersona(persona);
    var respuesta, body;
    body = response.body;
    if (respuestaEncriptada) {
      respuesta = decryptAESCryptoJS(response.body, secretKeyApi);
      body = json.decode(respuesta);
    } else {
      body = json.decode(body);
    }
    Navigator.pop(context);
    if (response.statusCode == 200 && body['ok'] == peticionCorrecta) {
      await guardarLogin(body);
       Navigator.popAndPushNamed(context,HomeScreen.routeName);
    }
    else if (body['ok'] == otpCrearCUentaCode){
      var codigo = '';
      validarOtpInicio(codigo,context,body);
    }
    else{      
      mostrarMensaje(body['mensaje'], context, tiempoToast);
    }
  } on Exception catch (e) {
    print(e);
    Navigator.pop(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
  }
}
