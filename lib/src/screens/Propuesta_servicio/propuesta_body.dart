import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movil_niniera/src/Config/configSharedPreferences.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/screens/Propuesta_servicio/propuesta_controller.dart';
import 'package:movil_niniera/src/size_config.dart';

class PropuestaBody extends StatefulWidget {
  @override
  _PropuestaBodyState createState() => _PropuestaBodyState();
}

class _PropuestaBodyState extends State<PropuestaBody> {

  var propuestas; 
  var datosId;
  getPropuesta() async {
    await Future.delayed(Duration(milliseconds: 2));
    mostrarLoading(context,'Cargando...');
    var data  = await controllerGetPropuestas(context);
    print(data);
    if(data.length>0){
      propuestas = data;
      datosId = data[0];
    }
    print('DATOSID $datosId');
    setState(() {});
  }

  @override
  void initState() {
    getPropuesta();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(      
      child: 
      
      propuestas!=null 
      ?Container(
        child: ListView.builder(
          itemCount: propuestas.length,
          itemBuilder: (_, i) {
            return Padding(
              padding: EdgeInsets.only(top: 20),
              child: Card(
                clipBehavior: Clip.antiAlias,
                elevation:5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(height: getProportionateScreenHeight(15),),
                    ListTile(
                        leading: mostrarImagen(propuestas[i]['persona'][0]['photo'], context),
                        title: Text(propuestas[i]['persona'][0]['first_name'].toString() + " " +propuestas[i]['persona'][0]['last_name'].toString()),
                        ),
                    SizedBox(height: getProportionateScreenHeight(15),),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        GestureDetector(
                          onTap: ()async {
                            mostrarLoading(context, 'Enviando...');
                            // var body = await enviarEmailValidar(context,setState);
                            // if(body != null){
                              var datos = await obtenerSesion();
                              var per = json.decode(datos);
                              var detalle = {
                                'id_niniera':propuestas[i]['id_niniera'].toString(),
                                'id_solicitud':propuestas[i]['id_solicitud'].toString(),
                                'id_familia':per['persona']['identificador'],
                              };
                              obtenerDetalleContrato(detalle, context);
                            //   ingresarCodigo(context, setState,crear);
                            // }
                          },
                          child: Container(
                            width: 100,
                            height: 30,
                            color: kPrimaryColor,
                            padding: EdgeInsets.only(top: 4),
                            child: Text(
                              'Contratar',
                              style: TextStyle(color: Colors.white),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: 8.0),
                          child: GestureDetector(
                            onTap: () {
                              mostrarLoading(context, 'Enviando...');
                              obtenerInfo(propuestas[i]['id_niniera'],context);
                            },
                            child: Container(
                              width: 100,
                              padding: EdgeInsets.only(top: 4),
                              height: 30,
                              color: kPrimaryColor,
                              child: Text('Ver más',
                                  style: TextStyle(color: Colors.white),
                                  textAlign: TextAlign.center),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      )
      : Container(
        alignment: Alignment.center,
        child: Center(
          child: Text('Aun no han propuesto o no lo has solicitado el servicio',textAlign: TextAlign.center)))
    );
  }
}
