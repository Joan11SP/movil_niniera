import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:movil_niniera/src/Config/config.dart';
import 'package:movil_niniera/src/Config/configSharedPreferences.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/components/default_button.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/screens/Propuesta_servicio/propuesta_controller.dart';
import 'package:movil_niniera/src/size_config.dart';

class DetalleContrato extends StatefulWidget {
  static String routeName = '/detalle-contrato';

  @override
  _DetalleContratoState createState() => _DetalleContratoState();
}

class _DetalleContratoState extends State<DetalleContrato> {
  var detalle;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    detalle = ModalRoute.of(context).settings.arguments;
    return Scaffold(
        backgroundColor: Colors.grey[300],
        appBar: AppBar(
          centerTitle: true,
          title: Text('Detalle', style: stiloText()),
          backgroundColor: kPrimaryColor,
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(20))),
                        child: mostrarImagen(detalle['foto'], context),
                        height: getProportionateScreenHeight(200),
                        width: getProportionateScreenWidth(150),
                      ),
                    ),
                    SizedBox(width: getProportionateScreenWidth(15)),
                    Container(
                        child: Text(
                          detalle['nombres'].toString(),
                          style: estiloText(),
                        ),
                        padding: EdgeInsets.only(
                            left: getProportionateScreenWidth(20)),
                        width: getProportionateScreenWidth(152))
                  ],
                ),
                mostrarInformacion(
                    Icons.hourglass_full,
                    'Tipo de pago',
                    'Efectivo'),
                mostrarInformacion(
                    Icons.hourglass_bottom_rounded,
                    'Total de horas de servicio',
                    detalle['n_horas'].toString()),
                mostrarInformacion(Icons.calendar_today, 'Total de dìas',
                    detalle['numero_dias'].toString()),
                mostrarInformacion(Icons.monetization_on_rounded,
                    'Precio por hora', detalle['precio_hora'].toString()),
                mostrarInformacion(Icons.local_offer, 'Descuento',
                    detalle['descuento'].toString() + " %"),
                mostrarInformacion(Icons.monetization_on_outlined, 'Total',
                    '\$'+detalle['total'].toString()),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 35, right: 35, bottom: 25),
                  child: DefaultButton(
                      press: () => enviarContrato(), text: 'Confirmar'),
                )
              ],
            ),
          ),
        ));
  }

  mostrarInformacion(IconData icon, String titulo, String detalle) {
    return Padding(
      padding: EdgeInsets.only(left: 15, right: 15),
      child: ListTile(
        //leading: Icon(Icons.monetization_on_outlined, color: kPrimaryColor),
        title: Text(titulo),
        trailing: Text(detalle),
      ),
    );
  }

  enviarContrato() async {
    mostrarLoading(context, 'Enviando...');

    var datos = await obtenerSesion();
    var per = json.decode(datos);
    var crear = {
        'id_niniera': detalle['id_niniera'].toString(),
        'id_solicitud': detalle['id_solicitud'].toString(),
        'id_familia': per['persona']['identificador'],
    };
    if(enviarCodigo){
      var body = await enviarEmailValidar(context,setState);
      if(body != null){      
        ingresarCodigo(context, setState,crear);
      }
    }else{
      crearServicio(crear, context,false); 
    }
  }
}
