import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:movil_niniera/src/Api/apiPersona.dart';
import 'package:movil_niniera/src/Config/config.dart';
import 'package:movil_niniera/src/Config/configEncrypt.dart';
import 'package:movil_niniera/src/Config/configSharedPreferences.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/screens/Propuesta_servicio/detalle_contrato.dart';
import 'package:movil_niniera/src/screens/home/home_screen.dart';
import 'package:movil_niniera/src/screens/info_niniera/info_niniera_screen.dart';

controllerGetPropuestas(context) async {
  var propuestas = [];
  try {
    var persona = await obtenerSesion();
    var datos = json.decode(persona);
    http.Response response =
        await buscarPropuestas(datos['persona']['identificador']);
    var respuesta;

    if (respuestaEncriptada) {
      respuesta = decryptAESCryptoJS(response.body, secretKeyApi);
    }
    esconderLoading(context);
    respuesta = json.decode(response.body);
    propuestas = respuesta['respuesta'];
    return propuestas;
  } on Exception catch (e) {
    esconderLoading(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
    return propuestas = [];
  }
}

buscarMasInfoNiniera(String id, context) async {
  try {
    http.Response response = await infoNiniera(id);
    var respuesta, body;
    body = response.body;
    if (respuestaEncriptada) {
      body = decryptAESCryptoJS(body, secretKeyApi);
    }
    esconderLoading(context);
    respuesta = json.decode(body);
    var masinfo = respuesta['respuesta'];
    print(masinfo);
    return masinfo;
  } on SocketException catch (_) {
    esconderLoading(context);
    mostrarMensaje(sinInternet, context, tiempoToast);
  } on Exception catch (_) {
    esconderLoading(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
  }
}

obtenerInfo(id, context) async {
  var info = await buscarMasInfoNiniera(id, context);
  if (info != null) {
    Navigator.of(context)
        .pushNamed(InfoNinieraScreen.routeName, arguments: info);
  }
}

enviarEmailValidar(context, setState) async {
  var respuestaEmial;
  try {
    var datos = await obtenerSesion();
    var per = json.decode(datos);
    http.Response response =
        await pedirOtp(per['persona']['email'], 'validacion');
    var respuesta, body;
    body = response.body;
    if (respuestaEncriptada) {
      respuesta = decryptAESCryptoJS(response.body, secretKeyApi);
      body = json.decode(respuesta);
    } else {
      body = json.decode(body);
    }
    esconderLoading(context);
    if (response.statusCode == 200 && body['ok'] == peticionErronea) {
      mostrarMensaje(body['mensaje'], context, tiempoToast);
      return respuestaEmial = null;
    } else {
      return respuestaEmial = body;
    }
  } on Exception catch (e) {
    Navigator.pop(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
    return respuestaEmial = null;
  }
}

crearServicio(Map crear, context,bool cerrar) async {
  try {
    http.Response response = await crearServicioNiniera(crear);
    var respuesta, body;
    body = response.body;
    if (respuestaEncriptada) {
      respuesta = decryptAESCryptoJS(response.body, secretKeyApi);
      body = json.decode(respuesta);
    } else {
      body = json.decode(body);
    }
    esconderLoading(context);
    if (response.statusCode == 200 && body['ok'] == peticionCorrecta) {
      
      if(cerrar) esconderLoading(context);

      Navigator.popAndPushNamed(context, HomeScreen.routeName);
      mostrarMensaje(
          body['mensaje'] + ", Ya tienes acceso al chat", context, tiempoToast);
    } else {
      mostrarMensaje(body['mensaje'], context, tiempoToast);
    }
  } on Exception catch (e) {
    Navigator.pop(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
  }
}

obtenerDetalleContrato(Map detalle, context) async {
  try {
    http.Response response = await detalleContrato(detalle);
    var respuesta, body;
    body = response.body;
    if (respuestaEncriptada) {
      respuesta = decryptAESCryptoJS(response.body, secretKeyApi);
      body = json.decode(respuesta);
    } else {
      body = json.decode(body);
    }
    esconderLoading(context);
    if (response.statusCode == 200 && body['ok'] == peticionCorrecta) {
      if (body['respuesta'] != null) {
        Navigator.pushNamed(context, DetalleContrato.routeName,
            arguments: body['respuesta']);
      } else {
        mostrarMensaje(body['mensaje'], context, tiempoToast);
      }
    } else {
      mostrarMensaje(body['mensaje'], context, tiempoToast);
    }
  } on Exception catch (e) {
    Navigator.pop(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
  }
}
