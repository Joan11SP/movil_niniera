import 'package:flutter/material.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/screens/Propuesta_servicio/propuesta_body.dart';
import 'package:movil_niniera/src/size_config.dart';

class PropuestaScreen extends StatelessWidget {
  static String routeName = '/propuesta_lista';
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 15.0,
        centerTitle: true,
        backgroundColor: kPrimaryColor,
        title: Text('Propuestas de Servicio',style: stiloText(),),
      ),
      body: PropuestaBody(),
    );
  }
}