import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:movil_niniera/src/Api/apiPersona.dart';
import 'package:movil_niniera/src/Config/config.dart';
import 'package:movil_niniera/src/Config/configEncrypt.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/screens/Ubicaciones/ubicaciones_screen.dart';
import 'package:movil_niniera/src/screens/home/home_screen.dart';

controllerUbicacion(ubicacion,context) async {
  try {
    http.Response response = await crearUbicacion(ubicacion);    
    var respuesta;
    var body = response.body;
    if(respuestaEncriptada){
      respuesta = decryptAESCryptoJS(response.body, secretKeyApi);
      body = respuesta;
    }
    respuesta = json.decode(body);
  
    Navigator.pop(context);
    if(response.statusCode == 200 && respuesta['ok'] == peticionCorrecta){
      Navigator.of(context).popAndPushNamed(HomeScreen.routeName);
      
    }else{
      mostrarMensaje(respuesta['mensaje'], context, tiempoToast);
    }
  } on Exception catch (e) {
    print('ERROR => $e');Navigator.pop(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
  }

}
controllerActualizarUbicacion(ubicacion,context) async {
  try {
    http.Response response = await actualizarUbicacion(ubicacion);    
    var respuesta;
    var body = response.body;
    if(respuestaEncriptada){
      respuesta = decryptAESCryptoJS(response.body, secretKeyApi);
      body = respuesta;
    }
    respuesta = json.decode(body);
  
    Navigator.pop(context);
    if(response.statusCode == 200 && respuesta['ok'] == peticionCorrecta){
      Navigator.of(context).popAndPushNamed(Ubicaciones.routeName);      
    }else{
      mostrarMensaje(respuesta['mensaje'], context, tiempoToast);
    }
  } on Exception catch (e) {
    print('ERROR => $e');Navigator.pop(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
  }

}