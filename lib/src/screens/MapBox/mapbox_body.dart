import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mapbox_gl/mapbox_gl.dart';
import 'package:movil_niniera/src/Config/config.dart';
import 'package:movil_niniera/src/Config/configSharedPreferences.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/components/default_button.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/helper/keyboard.dart';
import 'package:movil_niniera/src/screens/MapBox/mapbox_controller.dart';
import 'package:movil_niniera/src/size_config.dart';
import 'package:location/location.dart' as loc;

class BodyMapa extends StatefulWidget {
  @override
  _BodyMapaState createState() => _BodyMapaState();
}

class _BodyMapaState extends State<BodyMapa> {
  MapboxMapController mapController;
  double latitud, longitud, markerLatitud, markerLongitud;
  String referencias, calles;
  var ubicacionGuardada;
  String marker = 'assets/images/ubi.png';
  bool gpsActivado = false;
  loc.Location location = loc.Location();
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    checkGps();
    super.initState();
  }

  @override
  void dispose() {
    if (latitud != null) {
      mapController.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ubicacionGuardada = ModalRoute.of(context).settings.arguments;
    return latitud != null
        ? Container(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: viewMap(),
                    height: getProportionateScreenHeight(470),
                  ),
                  escribirUbicacion()
                ],
              ),
            ),
          )
        : Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(40)),
            child: Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                gpsActivado == false
                    ? DefaultButton(
                        text: 'Activar ubicación',
                        press: () async {
                          await checkGps();
                        },
                      )
                    : Center(child: CircularProgressIndicator(strokeWidth: 1))
              ],
            )),
          );
  }

  addSymbolMap([coordinates]) async {
    mapController.addSymbol(SymbolOptions(
        zIndex: 1,
        geometry: LatLng(
            coordinates.latitude,
            coordinates
                .longitude), // location is 0.0 on purpose for this example
        iconImage: marker,
        iconSize: 0.5));
    await mapController.clearSymbols();
  }

  viewMap() {
    return MapboxMap(
      onStyleLoadedCallback: () {
        mapController.addSymbol(SymbolOptions(
            geometry: LatLng(latitud, longitud),
            iconImage: marker,
            iconSize: 0.5,
            iconColor: 'blue'));
      },
      initialCameraPosition:
          CameraPosition(target: LatLng(latitud, longitud), zoom: 16.5),
      styleString: MapboxStyles.MAPBOX_STREETS,
      compassEnabled: false,
      rotateGesturesEnabled: false,
      onMapClick: (point, coordinates) {
        addSymbolMap(coordinates);        
        mostrarMensaje('Se tomó la posición marcada en el mapa', context, 3);
        setState(() {
          markerLatitud = coordinates.latitude;
          markerLongitud = coordinates.longitude;
        });
      },
      onMapCreated: onMapCreated,
    );
  }

  void onMapCreated(MapboxMapController controller) {
    mapController = controller;
  }

  escribirUbicacion() {
    return Container(
      height: getProportionateScreenHeight(300),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 18, horizontal: 15),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                TextFormField(
                  initialValue: ubicacionGuardada == null
                      ? ''
                      : ubicacionGuardada['ubi_calles'].toString(),
                  style: TextStyle(color: Colors.black87),
                  decoration: InputDecoration(
                      hintText: "Calles", focusColor: kPrimaryColor),
                  onSaved: (String valor) {
                    setState(() {
                      calles = valor;
                    });
                  },
                ),
                SizedBox(
                  height: 5,
                ),
                TextFormField(
                  initialValue: ubicacionGuardada == null
                      ? ''
                      : ubicacionGuardada['ubi_referencia'].toString(),
                  style: TextStyle(color: Colors.black87),
                  decoration: InputDecoration(
                      hintText: "Referencia", focusColor: kPrimaryColor),
                  onSaved: (String valor) {
                    setState(() {
                      referencias = valor;
                    });
                  },
                ),
                SizedBox(height: getProportionateScreenHeight(10)),
                DefaultButton(
                  text: ubicacionGuardada == null
                      ? "Registrar ubicación"
                      : 'Actualizar ubicación',
                  press: () {
                    KeyboardUtil.hideKeyboard(context);
                    _formKey.currentState.save();
                    obtenerDatosUbicacion();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future checkGps() async {
    try {
      if (!await location.serviceEnabled()) {
        await location.requestService();
        if (await location.serviceEnabled()) {
          datosModificar();
        }
      } else {
        datosModificar();
      }
    } on Exception catch (e) {
      print('ERROR $e');
    }
  }

  datosModificar() {
    if (ubicacionGuardada != null) {
      setState(() {
        gpsActivado = true;
        latitud = ubicacionGuardada['ubi_latitud'];
        longitud = ubicacionGuardada['ubi_longitud'];
      });
    } else {
      setState(() {
        gpsActivado = true;
        position();
      });
    }
  }

  position() async {
    try {
      var ubicacion = await location.getLocation();

      if (mounted) {
        setState(() {
          latitud = ubicacion.latitude;
          longitud = ubicacion.longitude;
        });
      }
    } on Exception catch (e) {
      print(e);
    }
  }

  obtenerDatosUbicacion() async {
    if (latitud != null && longitud != null && _formKey.currentState.validate()) {
      var perfil = await obtenerSesion();
      var datos = json.decode(perfil);
      mostrarLoading(context,'Enviando...');
      Map ubicacion = {
        'id_persona': datos['persona']['identificador'],
        'latitud': markerLatitud == null ? latitud : markerLatitud,
        'longitud': markerLongitud == null ? longitud : markerLongitud,
        'referencia': referencias,
        'calles': calles
      };
      if (ubicacionGuardada == null) {
        controllerUbicacion(ubicacion, context);
      } else {
        ubicacion['id_ubicacion'] = ubicacionGuardada['_id'];
        controllerActualizarUbicacion(ubicacion, context);
      }
    } else {
      mostrarMensaje(camposVacios, context, 3);
    }
  }
}
