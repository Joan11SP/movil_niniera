import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/screens/MapBox/mapbox_body.dart';
import 'package:movil_niniera/src/size_config.dart';

class MapaScreen extends StatelessWidget {
  static String routeName= '/registrar_ubicacion';
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
          elevation: 15.0,
          centerTitle: true,
          backgroundColor: kPrimaryColor,
          title: Text('Añade la información', style: stiloText(),)
      ),
      body: BodyMapa(),
    );
  }
}