import 'package:flutter/material.dart';
import 'package:movil_niniera/src/Services/provider.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/size_config.dart';

import 'home_body.dart';

class HomeScreen extends StatelessWidget {
  static String routeName = "/home";
  static var img;
  final ProviderInfor providerInfor = ProviderInfor();
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(      
      appBar: AppBar(
        elevation: 15.0,
        centerTitle: true,
        backgroundColor: kPrimaryColor,
        title: Text('Nanny', style: stiloText(),),
      ),
      body: SafeArea(child: Body()),      
      drawer: SafeArea(        
        child: menu(context, null),
      ),
    );
  }
}
