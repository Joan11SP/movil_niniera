import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:movil_niniera/src/Api/apiPersona.dart';
import 'package:movil_niniera/src/Config/config.dart';
import 'package:movil_niniera/src/Config/configEncrypt.dart';
import 'package:movil_niniera/src/Config/configSharedPreferences.dart';
import 'package:movil_niniera/src/components/botones.dart';

controllerGetNinieras(context) async {
  var ninieras=[];
  try {
    var persona = await obtenerSesion();
    var datos = json.decode(persona);
    http.Response response = await encontrarAllNinieras(datos['persona']['identificador']);    
    var respuesta,body;
    body = response.body;
    if(respuestaEncriptada){
      body = decryptAESCryptoJS(body, secretKeyApi);
    }
    esconderLoading(context);
    respuesta = json.decode(body);
    ninieras = respuesta['respuesta'];
    return ninieras;
  } on Exception catch (e) {
    esconderLoading(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
    return ninieras=[];
  }

}
