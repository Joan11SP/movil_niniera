import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:movil_niniera/src/Api/apiPersona.dart';
import 'package:movil_niniera/src/Services/firebase_notificacion.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/screens/Propuesta_servicio/propuesta_controller.dart';
import 'package:movil_niniera/src/screens/Propuesta_servicio/propuesta_screen.dart';
import 'package:movil_niniera/src/screens/Solicitudes/solicitudes_screen.dart';
import 'package:movil_niniera/src/screens/home/home_controller.dart';
import 'package:movil_niniera/src/size_config.dart';
import '../../Config/config.dart';
class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  var ninieras = [];
  String iconStar = 'assets/icons/star.svg';
  final notificacion = new Notificacion();
  getNinieras() async {
    await Future.delayed(Duration(milliseconds: 1));
    mostrarLoading(context, 'Cargando...');
    ninieras = await controllerGetNinieras(context);
    setState(() {});
  }

  @override
  void initState() {
    getNinieras();
    notificacion.initNotifications(context);
    notificacion.mensaje.listen((data) {
      if(data['pantalla'] == 'propuesta'){
        navigatorKey.currentState.pushNamed(PropuestaScreen.routeName);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SizedBox(height: getProportionateScreenHeight(20)),
          Padding(
            padding: EdgeInsets.only(right: 6.0),
            child: Align(
              child: FlatButton(onPressed: () async {
                
              },
              child: Text('Ver más',style: estiloText(),)),
              alignment: Alignment.topRight,
            ),
          ),
          SizedBox(height: 20,),
          SizedBox(
            height: getProportionateScreenHeight(350),
            child: ListView.builder(
              physics: ClampingScrollPhysics(),
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: ninieras.length,
              itemBuilder: (_, int i) => Container(
                width: getProportionateScreenWidth(250),
                child: GestureDetector(
                  child: Card(
                    child: Column(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(50))),
                          height: getProportionateScreenHeight(200),
                          width: getProportionateScreenWidth(150),
                          child: mostrarImagen(ninieras[i]['photo'], context),
                        ),
                        SizedBox(height: 15),
                        Text(ninieras[i]['nombres']),
                        SizedBox(height: 15),
                        ninieras[i]['puntuacion'] != null
                        ? Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            ...List.generate(
                              ninieras[i]['puntuacion']
                              , (index) => SvgPicture.asset(iconStar, height: 25)),
                            ...List.generate(
                              5 - ninieras[i]['puntuacion']
                              , (index) => SvgPicture.asset(iconStar, height: 25,color: Colors.grey[400],))
                          ],
                        ):Text('')
                      ],
                  ),                    
                ),
                onTap: () {
                  mostrarLoading(context, 'Enviando...');
                  obtenerInfo(ninieras[i]['id_niniera'],context);
                },
              ),
            ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Expanded(
                    flex: 2, child: Text('¿Necesitas solicitar una niñera?')),
                Expanded(
                    child: FlatButton(
                        onPressed: () => Navigator.pushNamed(
                            context, SolicitudesScreen.routeName),
                        child: Text('Click aqui')))
              ],
            ),
          )
        ],
      ),
    );
  }
}
