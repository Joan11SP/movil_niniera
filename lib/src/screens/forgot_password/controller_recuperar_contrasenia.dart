import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:movil_niniera/src/Api/apiPersona.dart';
import 'package:movil_niniera/src/Config/config.dart';
import 'package:movil_niniera/src/Config/configEncrypt.dart';
import 'package:movil_niniera/src/Config/configSharedPreferences.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/screens/otp/otp_screen.dart';

enviarEmailValidar(String email,context) async {
  try {
    http.Response response = await pedirOtp(email,'validacion');
    var respuesta, body;
    body = response.body;
    if (respuestaEncriptada) {
      respuesta = decryptAESCryptoJS(response.body, secretKeyApi);
      body = json.decode(respuesta);
    } else {
      body = json.decode(body);
    }
    Navigator.pop(context);
    if (response.statusCode == 200 && body['ok'] == peticionCorrecta) {
       await guardarDatosCambiarContrasenia(body['respuesta']);
       Navigator.popAndPushNamed(context,OtpScreen.routeName, 
       arguments: {
         'id':body['respuesta']['id'],
         'ruta':recuperarContrasenia
       });
    }else{      
      mostrarMensaje(body['mensaje'], context, tiempoToast);
    }
  } on Exception catch (e) {
    Navigator.pop(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
  }
}