import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movil_niniera/src/Services/chat_socket.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/screens/Chat/chat_enviar_mensaje.dart';
import 'package:movil_niniera/src/screens/Chat/chat_recibir_mensaje.dart';
import 'package:provider/provider.dart';

class ChatBoyd extends StatefulWidget {
  @override
  _ChatBoydState createState() => _ChatBoydState();
}

class _ChatBoydState extends State<ChatBoyd> {
  bool _showBottom = false;
  var messages = [];

  @override
  void initState() {
    
    final socketService = Provider.of<ChatService>(context,listen: false);

    socketService.scoket.on('nuevo-mensaje', (data){
      setState(() {});
    });
    
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Positioned.fill(
          child: Column(
            children: <Widget>[
              messages.length > 0
                  ? Expanded(
                      child: ListView.builder(
                        padding: const EdgeInsets.all(15),
                        itemCount: messages.length,
                        itemBuilder: (ctx, i) {
                          if (messages[i]['status'] == 'recivir') {
                            return RecibirMensaje(mensaje: messages[i]);
                          } else {
                            return EnviarMensaje(mensaje: messages[i]);
                          }
                        },
                      ),
                    )
                  : Center(child: Text('')),
              Container(
                margin: EdgeInsets.all(15.0),
                height: 61,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(35.0),
                          boxShadow: [
                            BoxShadow(
                                offset: Offset(0, 3),
                                blurRadius: 5,
                                color: Colors.grey)
                          ],
                        ),
                        child: Row(
                          children: <Widget>[
                            IconButton(
                                icon: Icon(Icons.face), onPressed: () {}),
                            Expanded(
                              child: TextField(
                                decoration: InputDecoration(
                                    hintText: "Type Something...",
                                    border: InputBorder.none),
                              ),
                            ),
                            IconButton(
                              icon: Icon(Icons.photo_camera),
                              onPressed: () {},
                            ),
                            IconButton(
                              icon: Icon(Icons.attach_file),
                              onPressed: () {},
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(width: 15),
                    Container(
                      padding: const EdgeInsets.all(15.0),
                      decoration: BoxDecoration(
                          color: kPrimaryColor, shape: BoxShape.circle),
                      child: InkWell(
                        child: Icon(
                          Icons.keyboard_voice,
                          color: Colors.white,
                        ),
                        onLongPress: () {
                          setState(() {
                            _showBottom = true;
                          });
                        },
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
        Positioned.fill(
          child: GestureDetector(
            onTap: () {
              setState(() {
                _showBottom = false;
              });
            },
          ),
        ),
        _showBottom
            ? Positioned(
                bottom: 90,
                left: 25,
                right: 25,
                child: Container(
                  padding: EdgeInsets.all(25.0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(0, 5),
                          blurRadius: 15.0,
                          color: Colors.grey)
                    ],
                  ),
                  child: GridView.count(
                    mainAxisSpacing: 21.0,
                    crossAxisSpacing: 21.0,
                    shrinkWrap: true,
                    crossAxisCount: 3,
                    children: List.generate(
                      icons.length,
                      (i) {
                        return Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15.0),
                            color: Colors.grey[200],
                            //border: Border.all(color: myGreen, width: 2),
                          ),
                          child: IconButton(
                            icon: Icon(
                              icons[i],
                              color: kPrimaryColor,
                            ),
                            onPressed: () {},
                          ),
                        );
                      },
                    ),
                  ),
                ),
              )
            : Container(),
      ],
    );
  }

  List<IconData> icons = [
    Icons.image,
    Icons.camera,
    Icons.file_upload,
    Icons.folder,
    Icons.gif
  ];
}
