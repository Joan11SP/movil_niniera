import 'dart:io';
import 'package:movil_niniera/src/helper/keyboard.dart';
import 'package:movil_niniera/src/screens/Chat/chat_enviar_mensaje.dart';
import 'package:movil_niniera/src/screens/Chat/chat_recibir_mensaje.dart';
import 'package:path/path.dart' as path;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:movil_niniera/src/Services/chat_socket.dart';
import 'package:movil_niniera/src/Services/provider.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/screens/profile/profile_controller.dart';
import 'package:movil_niniera/src/size_config.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import '../../components/botones.dart';

class ChatScreen extends StatefulWidget {
  static String routeName = 'chat-screen';
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  var messages = [];
  Map perfil;
  String idUser;
  var fecha = DateTime.now();
  var hora = TimeOfDay.now();
  int send = 1;
  bool escribiendo=false;
  bool imagenOrString = false;
  File imagen;
  Map enviarimagen = {};
  bool obtenerDatos = false;
  final picker = ImagePicker();
  TextEditingController mensaje = new TextEditingController();
  String typing;
  //eventos de socket
  String nuevoMensaje = 'nuevo-mensaje';
  String recibirMensaje = 'recibir-mensaje';
  String pedirChat = 'filter-chat-private';
  String chatsEncontrados = 'found-chat';
  String esEscribiendo = 'es-escribiendo';
  String emEscribiendo = 'em-escribiendo';

  ChatService socketService;

  var provider;

  @override
  void initState() {
    this.socketService = Provider.of<ChatService>(context, listen: false);
    provider = Provider.of<ProviderInfor>(context,listen: false);
    
    socketService.scoket.on(nuevoMensaje, (data){
      nuevoMensajeList(data,provider);
    });
    socketService.scoket.on(chatsEncontrados, mensajeEncontrado);    
    socketService.scoket.on(emEscribiendo, personaEscribiendo);

    super.initState();
  }
  
  nuevoMensajeList(data,provider) {
    setState(() {
      messages.insert(0, data['m']);
    });
    provider.mostrarUltimoMensaje(data['m']['message'],data['m']['emisor']);
  }

  mensajeEncontrado(data) {
    if (data != null) {
      setState(() {      
        messages.insertAll(0,data);
      });      
    }
  }

  enviarMensajeNiniera(socket, mensaje) {
    socket.scoket.emit(recibirMensaje, mensaje);

  }

  personaEscribiendo(data){
    setState(() {
      escribiendo = data['escribiendo'];
    });
  }
  @override
  Widget build(BuildContext context) {
    this.socketService = Provider.of<ChatService>(context);

    obtenerPerfil();    
    provider = Provider.of<ProviderInfor>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 15.0,
        backgroundColor: kPrimaryColor,
        title: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            mostrarImagen(perfil['photo'],context),            
            SizedBox(width: 15),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  perfil['first_name'] + " " + perfil['last_name'],
                  style: stiloText(15),
                  overflow: TextOverflow.clip,
                ),
                escribiendo 
                    ? Text(
                        "escribiendo...",                        
                        style: stiloText(13),
                      )
                    : Text('')
              ],
            )
          ],
        ),
        actions: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 10),
            child: socketService.chatStatus == ChatStatus.online
                ? Icon(
                    Icons.check_circle,
                    color: Colors.white,
                  )
                : Icon(
                    Icons.check_circle,
                    color: Colors.grey,
                  ),
          ),
        ],
      ),
      body: Container(
        child: Column(children: <Widget>[
          Flexible(
              child: 
              ListView.builder(
                padding: const EdgeInsets.all(15),
                itemCount: messages.length,
                itemBuilder: (_, i) {
                  if (messages[i]['receptor'] == idUser) {
                    //print(messages[i]);
                    return RecibirMensaje(mensaje:messages[i]);
                  } else {
                    return EnviarMensaje(mensaje:messages[i]);
                  }
                },
                reverse: true,
              )
            ),
          
          Container(
              margin: EdgeInsets.symmetric(horizontal: 8.0),
              child: Row(
                    children: <Widget>[
                    Flexible(
                        child: TextField(
                            controller: mensaje,
                            decoration: InputDecoration(hintText: "Escribe aquí..."),
                            onChanged: (value) {
                              eventotext(value);
                            },
                        )
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    enviarStringOImagen(provider),
                    Padding(padding: EdgeInsets.only(bottom: 10))
                  ],
                ))
        ]),
      ),
    );
  }

  Container enviarStringOImagen(provider) {
    return Container(
      width: getProportionateScreenWidth(55),
      padding: EdgeInsets.only(bottom:15.0,top: 15),
      decoration: BoxDecoration(
          color: kPrimaryColor, shape: BoxShape.circle),
      child: imagenOrString 
      ? IconButton(
          onPressed: () => enviarMensajePersona(socketService,provider),
          icon: Icon(Icons.send, color: Colors.white),
      )
      : IconButton(
          onPressed: () async{
            var status = await Permission.storage.status;
            if (status.isGranted) {
              selectImageGallery();
            } else {
              mostrarMensaje(
                  'Habilita los permisos de almacenamiento para continuar',
                  context,
                  3);
              pedirPersmisos();
            }
          },
          icon: Icon(Icons.camera_alt,color: Colors.white),
      )
    );
  }

  obtenerPerfil(){
    if(perfil == null){
      perfil = ModalRoute.of(context).settings.arguments;
      idUser = perfil['id_user'];
      print('SOLO SE DEBE INPRIMIR MIENTRAS NO TENGA LOS DATOS');
      this.socketService.scoket.emit(pedirChat, {'emisor': idUser, 'receptor': perfil['id_niniera']});
    }
  }
  
  obtenerHoraFecha() {
    setState(() {
      fecha = DateTime.now();
      hora = TimeOfDay.now();
    });
  }
  
  enviarMensajePersona(socketService,provider){
    obtenerHoraFecha();
    if (mensaje.text.toString().trim().length > 0) {
      Map mensajes = {
        'emisor': idUser,
        'receptor': perfil['id_niniera'],
        'mimetype':'string',
        'message': mensaje.text,
        'date': fecha.year.toString()+"-" +fecha.month.toString()+"-" +fecha.day.toString(),
        'hour': hora.hour.toString()+":" +hora.minute.toString()
      };      
      // provider.addMensaje(mensajes);
      setState(() {
        messages.insert(0,mensajes);
		    imagenOrString = false;		 
      });
      provider.mostrarUltimoMensaje(mensaje.text,idUser);
      enviarMensajeNiniera(socketService, mensajes);
      mensajes = null;
      mensaje.clear();
    }
  }
  
  enviarEscribiendo(bool type){
    socketService.scoket.emit(esEscribiendo, {
      'escribiendo': type,
      'receptor': perfil['id_niniera']
    });
  }
  
  eventotext(value){
    if(value != null && value.length > 0 && send==1){
      //enviarEscribiendo(true);
      setState(() {
        imagenOrString = true;
        send = 2;
      });
    }
    else if((value == null || value.length == 0 && send==2) ){
      //enviarEscribiendo(false);
      setState(() {
        imagenOrString = false;
        send = 1;
      });
    }
  }
  
  cancelar(){
    Navigator.pop(context);
    setState(() {
      imagen = null;
    });
  }
  
  enviarImagen()async{
    //KeyboardUtil.hideKeyboard(context);
    esconderLoading(context);
    mostrarLoading(context,'Enviando...');
    obtenerHoraFecha();
    var mensajes = {
        'emisor': idUser,
        'receptor': perfil['id_niniera'],
        'date': fecha.year.toString()+"-" +fecha.month.toString()+"-" +fecha.day.toString(),
        'hour': hora.hour.toString()+":" +hora.minute.toString(),
    };
    var datos = await enviarImagenChat(mensajes ,enviarimagen,context);
    if(datos == null){
      mostrarMensaje('No se pudo enviar la imagen', context, 3);
    }
    else{
      mensajes['message'] = imagen.path;
      mensajes['mimetype'] = datos['mimetype'];
      setState(() {
        messages.insert(0,mensajes);
      });
      print('DATOSSSSS $datos');
      enviarMensajeNiniera(socketService, {
        'message': datos['message'],
        'mimetype': datos['mimetype'],
        'date': mensajes['date'],
        'hour': mensajes['hour'],
        'emisor': idUser,
        'id':datos['id'],
        'receptor': perfil['id_niniera'],
      });
      mensajes = null;
    }
    
  }

  selectImageGallery() async {
    try {
      imagen = null;
      mostrarLoading(context, 'Cargando...');
      final img = await picker.getImage(source: ImageSource.gallery);
      setState(() {
        imagen = File(img.path);
        var extension = path.extension(imagen.path);
        enviarimagen = {'imagen': imagen.path, 'extension': extension};
      });
      esconderLoading(context);
      if (imagen != null) {
        subirImagenVarias(imagen,context,'Enviar imagen',20.0,cancelar,enviarImagen);
      }
    } on NoSuchMethodError catch (e) {      
      esconderLoading(context);
    } on PlatformException catch (e) {      
      esconderLoading(context);
    }
  }
  
  @override
  void dispose() {
    // Dejar de escuchar estos eventos.
    this.socketService.scoket.off(nuevoMensaje);
    this.socketService.scoket.off(chatsEncontrados);    
    this.socketService.scoket.off(emEscribiendo);
    enviarEscribiendo(false);
    super.dispose();
  }
}
