import 'dart:io';

import 'package:flutter/material.dart';
import 'package:movil_niniera/src/size_config.dart';

class EnviarMensaje extends StatelessWidget {
  final mensaje;
  const EnviarMensaje(
      {Key key, @required this.mensaje});

  @override
  Widget build(BuildContext context) {
    return  Padding(
      padding: EdgeInsets.only(top: 10),
      child: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Text(
              mensaje['hour'].toString(),
              style:
                  Theme.of(context).textTheme.body2.apply(color: Colors.grey),
            ),
            SizedBox(width: 15),
            
            mensaje['mimetype'] == 'string'?
            Container(
              constraints: BoxConstraints(
                  maxWidth: MediaQuery.of(context).size.width * .6),
              padding: const EdgeInsets.all(15.0),
              decoration: BoxDecoration(
                color: Colors.blueAccent,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25),
                  topRight: Radius.circular(25),
                  bottomLeft: Radius.circular(25),
                ),
              ),
              child: Text(
                mensaje['message'],
                style: Theme.of(context).textTheme.body2.apply(
                      color: Colors.white,
                    ),
              ),
            ):
            Container(
                  child: Image.file(File(mensaje['message'])),
                  constraints: BoxConstraints(
                      maxWidth: MediaQuery.of(context).size.width * .6),
                  padding: const EdgeInsets.all(15.0),
                  decoration: BoxDecoration(
                    color: Color(0xfff9f9f9),
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(25),
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    )),
              height: getProportionateScreenHeight(350),
              width: getProportionateScreenWidth(250),
                )
          ],
        ),
      ),
    );
  }
}