import 'dart:io';

import 'package:flutter/material.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/size_config.dart';


class RecibirMensaje extends StatelessWidget {
  final mensaje;
  const RecibirMensaje({@required this.mensaje});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 10),
      child: Row(
        children: <Widget>[
          SizedBox(height: 10),
          SingleChildScrollView(
            physics: ClampingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                mensaje['mimetype'] == 'string' ?
                Container(
                  constraints: BoxConstraints(
                      maxWidth: MediaQuery.of(context).size.width * .6),
                  padding: const EdgeInsets.all(15.0),
                  decoration: BoxDecoration(
                    color: Color(0xfff9f9f9),
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(25),
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    ),
                  ),
                  child: Text(mensaje['message'])
                )
                : mensaje['mimetype'] != 'string'?
                Container(
                  child: mostrarImagen(mensaje['message'], context),
                  constraints: BoxConstraints(
                      maxWidth: MediaQuery.of(context).size.width * .6),
                  padding: const EdgeInsets.all(15.0),
                  decoration: BoxDecoration(
                    color: Color(0xfff9f9f9),
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(25),
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    )),
                  height: getProportionateScreenHeight(200),
                  width: getProportionateScreenWidth(150),
                ):Container()
              ],
            ),
          ),
          SizedBox(width: 15),
          Text(
            mensaje['hour'].toString(),
          ),
        ],
      ),
    );
  }
}
