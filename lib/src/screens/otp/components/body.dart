import 'package:flutter/material.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/size_config.dart';

import 'otp_form.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Padding(
        padding:
            EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: SizeConfig.screenHeight * 0.05),
              Text(
                "Validación de código",
                style: headingStyle,
              ),
              buildTimer(),
              OtpForm(),
              SizedBox(height: SizeConfig.screenHeight * 0.1),
              GestureDetector(
                onTap: () {
                  // OTP code resend
                },
                child: Text(
                  "Reenviar código de validación",
                  style: TextStyle(decoration: TextDecoration.underline),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Row buildTimer() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("El código expira en 5 minutos"),
        // TweenAnimationBuilder(
        //   tween: Tween(begin: 30.0, end: 0.0),
        //   duration: Duration(minutes: 5),
        //   builder: (_, value, child) => Text(
        //     "00:${value.toInt()}",
        //     style: TextStyle(color: kPrimaryColor),
        //   ),
        // ),
      ],
    );
  }
}
