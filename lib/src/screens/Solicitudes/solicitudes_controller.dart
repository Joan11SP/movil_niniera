import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:movil_niniera/src/Api/apiPersona.dart';
import 'package:movil_niniera/src/Config/config.dart';
import 'package:movil_niniera/src/Config/configEncrypt.dart';
import 'package:movil_niniera/src/Config/configSharedPreferences.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/screens/home/home_screen.dart';

controllerGetUbicacionesNinios(context) async {
  try {
    var persona = await obtenerSesion();
    var datos = json.decode(persona);
    http.Response response = await buscarNiniosUbicaciones(datos['persona']['identificador']);    
    var respuesta;

    if(respuestaEncriptada){
      respuesta = decryptAESCryptoJS(response.body, secretKeyApi);
    }
    esconderLoading(context);
    respuesta = json.decode(response.body);
    var ubicaciones = respuesta['respuesta'];
    print(ubicaciones);
    return ubicaciones;
  } on Exception catch (e) {
    print('ERROR => $e');
    esconderLoading(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
  }

}

constrollerCrearSolicitud(Map solicitud,context)  async {
  try {
    mostrarLoading(context,'Cargando...');
    var persona = await obtenerSesion();
    var datos = json.decode(persona);
    solicitud['id_familia'] = datos['persona']['identificador'];
    http.Response response = await crearSolicitud(solicitud);    
    var respuesta;
    var body = response.body;
    if(respuestaEncriptada){
      body = decryptAESCryptoJS(body, secretKeyApi);      
    }
    esconderLoading(context);
    respuesta = json.decode(body);

    if(response.statusCode == 200 && respuesta['ok'] == peticionCorrecta){      
      Navigator.of(context).pushNamed(HomeScreen.routeName);
    }    
    mostrarMensaje(respuesta['mensaje'], context, 2);
  } on Exception catch (e) {
    print('ERROR => $e');
    mostrarMensaje(mensajeError, context, tiempoToast);
  }
}