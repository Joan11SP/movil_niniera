import 'package:flutter/material.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/screens/Solicitudes/solicitudes_body.dart';
import 'package:movil_niniera/src/size_config.dart';

class SolicitudesScreen extends StatelessWidget {
  static String routeName = '/solicitudes_screen';
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 15.0,
        centerTitle: true,
        backgroundColor: kPrimaryColor,
        title: Text('Solicitar Niñera',style: stiloText(),),
      ),
      body: SolicitudesForm(),
    );
  }
}