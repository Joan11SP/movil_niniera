import 'package:flutter/material.dart';
import 'package:movil_niniera/src/components/botones.dart';
import 'package:movil_niniera/src/components/custom_surfix_icon.dart';
import 'package:movil_niniera/src/components/default_button.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/helper/keyboard.dart';
import 'package:movil_niniera/src/screens/Ninios/ninios_screen.dart';
import 'package:movil_niniera/src/screens/Solicitudes/solicitudes_controller.dart';
import 'package:movil_niniera/src/screens/Ubicaciones/ubicaciones_screen.dart';
import 'package:movil_niniera/src/size_config.dart';

class SolicitudesForm extends StatefulWidget {
  const SolicitudesForm(
      {Key key,
      this.labelText,
      this.selectedDate,
      this.selectedTime,
      this.selectDate,
      this.selectTime})
      : super(key: key);

  final String labelText;
  final DateTime selectedDate;
  final TimeOfDay selectedTime;
  final ValueChanged<DateTime> selectDate;
  final ValueChanged<TimeOfDay> selectTime;

  @override
  _SolicitudesFormState createState() => _SolicitudesFormState();
}

class _SolicitudesFormState extends State<SolicitudesForm> {
  var ubicaciones;
  var dataUbicacionNinios;
  var ninios;
  var idNinios = [];
  String ubicacionId;
  String niniosId;
  bool buscar = true;
  TextEditingController diaInicio = TextEditingController();
  TextEditingController diaFin = TextEditingController();
  TextEditingController horaInicio = TextEditingController();
  TextEditingController horaFin = TextEditingController();
  TextEditingController idUbicacion = TextEditingController();
  TextEditingController descripcion = TextEditingController();

  Future<Null> _selectDate(TextEditingController dia) async {
    KeyboardUtil.hideKeyboard(context);
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime.now(),
        lastDate: DateTime(2101));
    if (picked != null)
      setState(() {
        dia.text = picked.year.toString() +
            "-" +
            picked.month.toString() +
            "-" +
            picked.day.toString();
      });
  }

  Future<Null> _selectTime(
      BuildContext context, TextEditingController hora) async {
    KeyboardUtil.hideKeyboard(context);
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );
    if (picked != null)
      setState(() {
        hora.text = picked.hour.toString() + ":" + picked.minute.toString();
        return hora;
      });
  }

  getNiniosUbicacion() async {
    await Future.delayed(Duration(milliseconds: 2));
    mostrarLoading(context, 'Cargando...');
    dataUbicacionNinios = await controllerGetUbicacionesNinios(context);
    if (dataUbicacionNinios != null) {
      ubicaciones = dataUbicacionNinios['ubi'];
      ninios = dataUbicacionNinios['boys'];
      if(ninios != null ){
        for (var i = 0; i < ninios.length; i++) {
          ninios[i]['status'] = false;
        }
      }
    }

    setState(() {});
  }

  @override
  void initState() {
    getNiniosUbicacion();
    super.initState();
  }

  alertUbicaciones() {
    return showDialog(
        context: context,
        child: AlertDialog(
          content: ListView.builder(
            shrinkWrap: true,
            itemCount: ubicaciones.length == null ? 0 : ubicaciones.length,
            itemBuilder: (context, i) {
              return GestureDetector(
                child: ListTile(
                  title: Text(ubicaciones[i]['ubi_calles'].toString()),
                  subtitle: Text(ubicaciones[i]['ubi_referencia'].toString()),
                  isThreeLine: true,
                ),
                onTap: () {
                  ubicacionId = ubicaciones[i]['_id'];
                  idUbicacion.text = ubicaciones[i]['ubi_calles'].toString();
                  setState(() {});
                  esconderLoading(context);
                },
              );
            },
          ),
        ));
  }

  agregarIdNinios() {
    idNinios.length = 0;
    for (var i = 0; i < ninios.length; i++) {
      if (ninios[i]['status']) {
        setState(() {
          idNinios.add(ninios[i]['_id']);
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              SizedBox(
                height: getProportionateScreenHeight(30),
              ),
              Container(
                child: Text('Día y fecha comienzo'),
                alignment: Alignment.topCenter,
              ),
              SizedBox(
                height: getProportionateScreenHeight(15),
              ),
              Row(
                children: [
                  new Expanded(
                      flex: 1,
                      child: GestureDetector(
                        onTap: () {
                          _selectDate(diaInicio);
                        },
                        child: TextFormField(
                          enabled: false,
                          controller: diaInicio,
                          decoration: InputDecoration(
                            floatingLabelBehavior: FloatingLabelBehavior.always,
                            suffixIcon: CustomSurffixIcon(
                                svgIcon: "assets/icons/Lock.svg"),
                          ),
                        ),
                      )),
                  const SizedBox(width: 6.0),
                  new Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () {
                        _selectTime(context, horaInicio);
                      },
                      child: TextFormField(
                        enabled: false,
                        controller: horaInicio,
                        decoration: InputDecoration(
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          suffixIcon: CustomSurffixIcon(
                              svgIcon: "assets/icons/Lock.svg"),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: getProportionateScreenHeight(20),
              ),
              Container(
                child: Text('Día y fecha fin'),
                alignment: Alignment.topCenter,
              ),
              SizedBox(
                height: getProportionateScreenHeight(15),
              ),
              Row(
                children: [
                  new Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () {
                        KeyboardUtil.hideKeyboard(context);
                        _selectDate(diaFin);
                      },
                      child: TextFormField(
                        controller: diaFin,
                        enabled: false,
                        decoration: InputDecoration(
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          suffixIcon: CustomSurffixIcon(
                              svgIcon: "assets/icons/Lock.svg"),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(width: 6.0),
                  new Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () {
                        _selectTime(context, horaFin);
                      },
                      child: TextFormField(
                        controller: horaFin,
                        enabled: false,
                        decoration: InputDecoration(
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          suffixIcon: CustomSurffixIcon(
                              svgIcon: "assets/icons/Lock.svg"),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: getProportionateScreenHeight(30),
              ),
              ubicaciones != null
                  ? GestureDetector(
                      onTap: () {
                        alertUbicaciones();
                      },
                      child: TextFormField(
                        controller: idUbicacion,
                        enabled: false,
                        decoration: InputDecoration(
                          labelText: 'Ubicación',
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          suffixIcon: CustomSurffixIcon(
                              svgIcon: "assets/icons/Location point.svg"),
                        ),
                      ),
                    )
                  : DefaultButton(
                      text: 'Agregar ubicación',
                      press: () {
                        Navigator.of(context).pushNamed(Ubicaciones.routeName);
                      },
                    ),
              SizedBox(
                height: getProportionateScreenHeight(30),
              ),
              ninios != null
                  ? Container(
                      child: Text('Agregar Niños'),
                      alignment: Alignment.topCenter,
                    )
                  : Text(''),
              ninios != null
                  ? Container(
                      child: ListView.builder(
                      shrinkWrap: true,
                      physics: const ClampingScrollPhysics(),
                      itemCount: ninios.length == null ? 0 : ninios.length,
                      itemBuilder: (context, i) {
                        return CheckboxListTile(
                            key: Key(i.toString()),
                            value: ninios[i]['status'],
                            activeColor: kPrimaryColor,
                            checkColor: Colors.white,
                            controlAffinity: ListTileControlAffinity.leading,
                            secondary: CustomSurffixIcon(
                                svgIcon: "assets/icons/User.svg"),
                            onChanged: (bool value) {
                              setState(() {
                                ninios[i]['status'] = value;
                                return ninios;
                              });
                            },
                            title: Text(ninios[i]['nombres'].toString()));
                      },
                    ))
                  : DefaultButton(
                      text: 'Agregar niños',
                      press: () {
                        Navigator.of(context).pushNamed(NiniosScreen.routeName);
                      },
                    ),
              SizedBox(
                height: getProportionateScreenHeight(30),
              ),
              TextFormField(
                maxLines: 3,
                controller: descripcion,
                decoration: InputDecoration(
                  labelText: "Descripcion",
                  hintText: "Ingresa una breve descripcion",
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  suffixIcon:
                      CustomSurffixIcon(svgIcon: "assets/icons/solicitar.svg"),
                ),
              ),
              SizedBox(
                height: getProportionateScreenHeight(30),
              ),
              DefaultButton(
                text: 'Solicitar',
                press: () {
                  KeyboardUtil.hideKeyboard(context);
                  agregarIdNinios();
                  obtenerDatos();
                },
              ),
              SizedBox(
                height: getProportionateScreenHeight(30),
              ),
            ],
          ),
        ),
      ),
    );
  }

  obtenerDatos() async {
    bool validar = diaInicio.text.length > 0 &&
        diaFin.text.length > 0 &&
        horaInicio.text.length > 0 &&
        horaFin.text.length > 0 &&
        descripcion.text.length > 0 &&
        idNinios.length > 0;
    if (validar) {
      Map solicitud = {
        'fecha_inicio': diaInicio.text,
        'fecha_fin': diaFin.text,
        'hora_inicio': horaInicio.text,
        'hora_fin': horaFin.text,
        'ubicacion': ubicacionId,
        'descripcion': descripcion.text,
        'id_ninios': idNinios
      };
      constrollerCrearSolicitud(solicitud, context);
    } else {
      mostrarMensaje('Completa la informacion', context, 2);
    }
  }
}
