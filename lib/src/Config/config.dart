import 'package:flutter/widgets.dart';

const String ip = '192.168.1.16' ;
const String urlSocket = 'http://fintech.kradac.com:4501/';
//const String urlSocket = 'http://$ip:$port/';
//const String urlApi = 'http://$ip:$portApi/api';
const String urlApi = 'http://fintech.kradac.com:4500/api';
const String secretKeySocket = '@ llave para encritar datos por sockets @';
const String secretKeyApi = "@ key para encriptar datos via api rest @";
const String mensajeError='Ocurrio un error, intenta más tarde';
const String registroPersona= '/completar-registro';
const String imagenPrueba = 'assets/images/Profile Image.png';
const String sinInternet = 'Sin conexión a internet, intente más tarde';
const String dominio = 'google.com';
const String recuperarContrasenia = '/recuperar-contrasenia';
const String camposVacios = 'Los campos no pueden quedar vacios';
const int portApi = 4500;
const int port = 4000;
const int peticionCorrecta= 1;
const int peticionErronea = 0;
const int tiempoToast = 2;
const bool respuestaEncriptada = false;
const bool enviarCodigo = false;
const int otpLoginCode = 9;
const int otpCrearCUentaCode = 8;
final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();
Map cabecera = {
  'app_nombre':'app_family',
  
};