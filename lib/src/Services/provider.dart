import 'package:flutter/cupertino.dart';

class ProviderInfor with ChangeNotifier {
  var foto;
  String _ultimoMensaje='';
  String _emisor= '';
  var _mensajesChat = [];
  List get allMensajes => _mensajesChat;

  String get ultimoMensaje => _ultimoMensaje;
  String get emisor => _emisor;

  addAllMensajes(data) {
    _mensajesChat = data;
    notifyListeners();
  }

  addMensaje(data) {
    _mensajesChat.add(data);
    notifyListeners();
  }

  //mostrar ultimo mensaje que se envio o recibio 
  mostrarUltimoMensaje(String mensaje,String emisor){
    this._ultimoMensaje = mensaje;
    this._emisor = emisor;
    notifyListeners();
  }
}
