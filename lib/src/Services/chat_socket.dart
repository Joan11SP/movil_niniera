import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:movil_niniera/src/Config/config.dart';
import 'package:movil_niniera/src/Config/configSharedPreferences.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

enum ChatStatus{
  connecting,
  online,
  offline
}

class ChatService with ChangeNotifier{

  ChatStatus _chatStatus = ChatStatus.connecting;
  IO.Socket _socket;
  ChatStatus get chatStatus => this._chatStatus;
  IO.Socket  get scoket => this._socket;
  ChatService(){
    this._initChatService();
  }
  
  void _initChatService(){
    this._socket = IO.io(urlSocket, {
    'transports': ['websocket'],
    'autoConnect':true
    });

    this._socket.on('connect', (data) async { 
      this._chatStatus = ChatStatus.online;
      print('CONECTADO =====> ${this._chatStatus}');
      var datos = await obtenerSesion();
      var per = json.decode(datos);
      String idUser = per['persona']['identificador'];
      if (idUser != null) {
        this._socket.emit('nuevo-usuario-conectado', {'session': idUser});        
      }
      notifyListeners();
    } );

    this._socket.on('disconnect', (data) { 
      this._chatStatus = ChatStatus.offline;
      print('DESCONECTADO =====> ${this._chatStatus}');
      notifyListeners();
    } );
  }




}