import 'dart:async';
import 'dart:convert';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:movil_niniera/src/screens/Propuesta_servicio/propuesta_screen.dart';
import '../Config/config.dart';
FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
List person;
class Notificacion {
  final streams = StreamController<dynamic>.broadcast();
  Stream<dynamic> get mensaje => streams.stream;
  var argumento;
  final localNotification = LocalNotification();
  navegar(msg) {
    argumento = msg['data'];
    streams.sink.add(argumento);
  }
  //token para reconocer cada dispositivo
  getTokenFmc() async {    
    _firebaseMessaging.requestNotificationPermissions();
    var token = await _firebaseMessaging.getToken();
    return token;
  }

  // escuchar cuando llega una notificacion
  initNotifications(context) async {
    _firebaseMessaging.requestNotificationPermissions();
    localNotification.init();

    _firebaseMessaging.configure(
      //dentro de la aplicacion

      onMessage: (msg) async {
        
        Map notificacion = {
          'titulo': msg['notification']['title'],
          'mensaje':msg['notification']['body'],
          'data':msg['data']
        };
        localNotification.mostrarNotificacion(notificacion);
        
        if(msg['data']['pantalla'] == 'propuesta')
          return navigatorKey.currentState.pushNamed(PropuestaScreen.routeName);
        navegar(msg['data']);
      },
      onLaunch: (msg) async {
        Map notificacion = {
          'titulo': msg['notification']['title'],
          'mensaje':msg['notification']['body'],
          'data':msg['data']
        };
        localNotification.mostrarNotificacion(notificacion);
        
        if(msg['data']['pantalla'] == 'propuesta')
          return navigatorKey.currentState.pushNamed(PropuestaScreen.routeName);
        navegar(msg['data']);
      },
      onResume: (msg) async {
        Map notificacion = {
          'titulo': msg['notification']['title'],
          'mensaje':msg['notification']['body'],
          'data':msg['data']
        };
        localNotification.mostrarNotificacion(notificacion);
        
        if(msg['data']['pantalla'] == 'propuesta')
          return navigatorKey.currentState.pushNamed(PropuestaScreen.routeName);
        navegar(msg['data']);
      },
    );
  }

  dispose() {
    streams.close();
  }
}
class LocalNotification {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
// initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
  AndroidInitializationSettings initializationSettingsAndroid = AndroidInitializationSettings('ic_launcher');
  
  init() async {
    final IOSInitializationSettings initializationSettingsIOS = IOSInitializationSettings(onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    final InitializationSettings initializationSettings = InitializationSettings(android: initializationSettingsAndroid,iOS: initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,onSelectNotification: selectNotification);
  }

  Future selectNotification(String payload) async {
    if (payload != null) {
      var datos = json.decode(payload);
      if(datos['pantalla'] == 'propuesta')
        return navigatorKey.currentState.pushNamed(PropuestaScreen.routeName);
    }
  }

  Future onDidReceiveLocalNotification(int i, String a, String b, String c) async {}

  mostrarNotificacion(Map data) async {
    const AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails(
            'your channel id', 'your channel name', 'your channel description',
            importance: Importance.max,
            priority: Priority.high,
            showWhen: false);
    const NotificationDetails platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        0, data['titulo'], data['mensaje'], platformChannelSpecifics,
        payload: json.encode(data['data']));
  }
}

