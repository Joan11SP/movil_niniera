import 'package:flutter/widgets.dart';
import 'package:movil_niniera/src/Config/config.dart';
import 'package:movil_niniera/src/screens/Chat/chat_bandeja.dart';
import 'package:movil_niniera/src/screens/Chat/chat_screen.dart';
import 'package:movil_niniera/src/screens/MapBox/mapbox_view.dart';
import 'package:movil_niniera/src/screens/Ninios/ninios_body.dart';
import 'package:movil_niniera/src/screens/Ninios/ninios_screen.dart';
import 'package:movil_niniera/src/screens/Propuesta_servicio/detalle_contrato.dart';
import 'package:movil_niniera/src/screens/Propuesta_servicio/propuesta_screen.dart';
import 'package:movil_niniera/src/screens/ServicioTerminado/servicio_calificar_screen.dart';
import 'package:movil_niniera/src/screens/Solicitudes/solicitudes_screen.dart';
import 'package:movil_niniera/src/screens/Ubicaciones/ubicaciones_screen.dart';
import 'package:movil_niniera/src/screens/cambiar_contrasenia/screen_recuperar_contrasenia.dart';
import 'package:movil_niniera/src/screens/complete_profile/complete_profile_screen.dart';
import 'package:movil_niniera/src/screens/forgot_password/forgot_password_screen.dart';
import 'package:movil_niniera/src/screens/home/home_screen.dart';
import 'package:movil_niniera/src/screens/info_niniera/info_niniera_screen.dart';
import 'package:movil_niniera/src/screens/otp/otp_screen.dart';
import 'package:movil_niniera/src/screens/profile/notificacion.dart';
import 'package:movil_niniera/src/screens/profile/perfil.dart';
import 'package:movil_niniera/src/screens/profile/profile_screen.dart';
import 'package:movil_niniera/src/screens/sign_in/sign_in_screen.dart';
import 'screens/sign_up/sign_up_screen.dart';

// We use name route
// All our routes will be available here
final Map<String, WidgetBuilder> routes = {
  SignInScreen.routeName: (context) => SignInScreen(),
  ForgotPasswordScreen.routeName: (context) => ForgotPasswordScreen(),
  SignUpScreen.routeName: (context) => SignUpScreen(),
  CompleteProfileScreen.routeName: (context) => CompleteProfileScreen(),
  OtpScreen.routeName: (context) => OtpScreen(),
  HomeScreen.routeName: (context) => HomeScreen(),
  ProfileScreen.routeName: (context) => ProfileScreen(),
  registroPersona:(context) => CompleteProfileScreen(),
  recuperarContrasenia: (context) => RecuperarContraseniaScreen(),
  MapaScreen.routeName: (context) => MapaScreen(),
  Ubicaciones.routeName:(context) => Ubicaciones(),
  NiniosScreen.routeName: (context) => NiniosScreen(),
  NiniosForm.routeName: (context) => NiniosForm(),
  SolicitudesScreen.routeName:(context) => SolicitudesScreen(),
  PropuestaScreen.routeName: (context) => PropuestaScreen(),
  InfoNinieraScreen.routeName:(context) => InfoNinieraScreen(),
  '/home':(context) => HomeScreen(),
  ChatScreen.routeName: (context) => ChatScreen(),
  BandejaChat.routeName : (context) => BandejaChat(),
  CalificarServicioScreen.routeName: (context) => CalificarServicioScreen(),
  PerfilScreen.routeName: (context) => PerfilScreen(),
  NotificacionScreen.routeName:(context) => NotificacionScreen(),
  DetalleContrato.routeName : (context) => DetalleContrato()
};
