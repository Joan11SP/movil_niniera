import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:movil_niniera/src/Config/config.dart';
import 'package:movil_niniera/src/Config/configSharedPreferences.dart';
import 'package:movil_niniera/src/components/default_button.dart';
import 'package:movil_niniera/src/constants.dart';
import 'package:movil_niniera/src/screens/Chat/chat_bandeja.dart';
import 'package:movil_niniera/src/screens/Ninios/ninios_screen.dart';
import 'package:movil_niniera/src/screens/Propuesta_servicio/propuesta_controller.dart';
import 'package:movil_niniera/src/screens/Propuesta_servicio/propuesta_screen.dart';
import 'package:movil_niniera/src/screens/Solicitudes/solicitudes_screen.dart';
import 'package:movil_niniera/src/screens/Ubicaciones/ubicaciones_screen.dart';
import 'package:movil_niniera/src/screens/profile/profile_screen.dart';
import 'package:movil_niniera/src/size_config.dart';
import 'package:toast/toast.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

var codigo;
String assetSolicitarServicio = "assets/icons/solicitar.svg";
String assetConfig = "assets/icons/Settings.svg";
String assetUbicacion = 'assets/icons/Location point.svg';
String assetNinios = 'assets/icons/User.svg';
String assetChat = 'assets/icons/chat.svg';

void mostrarLoading(BuildContext context, [String text]) {
  showDialog(
    //barrierColor: Colors.black.withOpacity(0.7),
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        content: Container(
          color: Colors.transparent,
          padding: EdgeInsets.all(10.0),
          child: new Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              new CircularProgressIndicator(
                backgroundColor: Colors.white,
                valueColor: colorLoading,
                strokeWidth: 1,
              ),
              SizedBox(
                height: 10.0,
              ),
              new Text(
                text,
                style: TextStyle(color: Colors.white, fontFamily: 'semibold'),
              ),
            ],
          ),
        ),
      );
    },
  );
}

esconderLoading(context) {
  Navigator.of(context).pop();
}

mostrarMensaje(String msg, context, int duracion) {
  return Toast.show(msg, context, duration: duracion);
}

ingresarCodigo(context, setState,Map crear) {
  return showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Ingrese el còdigo enviado a tu correo' ,style: estiloText(),),
        elevation: 0.0,
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            PinCodeTextField(
              showCursor: false,
              appContext: context,
              length: 6,
              obscureText: true,
              obscuringCharacter: '*',
              autoFocus: true,
              pastedTextStyle: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
              pinTheme: pintheme(),
              enableActiveFill: false,
              keyboardType: TextInputType.number,
              onCompleted: (v) {
                setState(() {
                  codigo = v;
                });
              },
              onChanged: (value) {
                print(value);
                setState(() {});
              },
            ),
            DefaultButton(
              text: 'Validar',
              press: () async {
                if(codigo.length == 6){
                  mostrarLoading(context,'Validando...');
                  crear['codigo'] = codigo;
                  crearServicio(crear, context,true); 
                }
              },
            )
          ],
        ),
      );
    },
  );
}

pintheme(){
  return PinTheme(
    inactiveColor: Colors.grey[400],
    activeColor: Colors.grey[400],
    disabledColor: Colors.grey[400],
    selectedColor: Colors.grey[400],
    shape: PinCodeFieldShape.box,
    borderRadius: BorderRadius.circular(5),
    fieldHeight: 50,
    fieldWidth: getProportionateScreenWidth(38),
  );
}

mostrarImagen(img, context) {
  return img != null
      ? CircleAvatar(
          foregroundColor: Theme.of(context).primaryColor,
          backgroundImage: NetworkImage(img),
          onBackgroundImageError: (context, StackTrace stackTrace) {
              return AssetImage(imagenPrueba);
            },
          )
      : CircleAvatar(
          radius: 25,
          foregroundColor: Theme.of(context).primaryColor,
          backgroundColor: Colors.grey,
          backgroundImage: AssetImage(imagenPrueba));
}

menu(context, imagen) {
  return ClipRRect(
    // give it your desired border radius
    borderRadius: BorderRadius.only(
        topRight: Radius.circular(50), bottomRight: Radius.circular(20)),
    child: Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(50))),
      width: getProportionateScreenWidth(270),
      child: Drawer(        
        elevation: 12.0,
        child: ListView(
          padding: EdgeInsets.only(left: 8.0),
          children: <Widget>[
            _createHeader(imagen),
            _createDrawerItem(
                assets: assetConfig,
                text: 'Más',
                onTap: () {
                  Navigator.of(context).pushNamed(ProfileScreen.routeName);
                }),
            _createDrawerItem(
                assets: assetUbicacion,
                text: 'Mis direcciones',
                onTap: () {
                  Navigator.of(context).pushNamed(Ubicaciones.routeName);
                }),
            _createDrawerItem(
                assets: assetNinios,
                text: 'Mis Niños',
                onTap: () {
                  Navigator.of(context).pushNamed(NiniosScreen.routeName);
                }),
            _propuestasServicio(
                text: 'Propuestas Servicio',
                onTap: () {
                  Navigator.of(context).pushNamed(PropuestaScreen.routeName);
                }),
            _createDrawerItem(
                assets: assetSolicitarServicio,
                text: 'Solicitar Servicio',
                onTap: () {
                  Navigator.of(context).pushNamed(SolicitudesScreen.routeName);
                }),
            _createDrawerItem(
                assets: assetChat,
                text: 'Chat',
                onTap: () {
                  Navigator.of(context).pushNamed(BandejaChat.routeName);
                }),
          ],
        ),
      ),
    ),
  );
}

_createDrawerItem({String assets,String text, GestureTapCallback onTap}) {
  return ListTile(
    title: Row(
      children: <Widget>[
        Container(
          child: SvgPicture.asset(
            assets,
            color: kPrimaryColor,
            height: 15,
          )
        ),
        Padding(
          padding: EdgeInsets.only(left: 8.0),
          child: Text(text),
        )
      ],
    ),
    onTap: onTap,
  );
}

_createHeader(imagen) {
  return DrawerHeader(
      child: FutureBuilder(
        future: obtenerSesion(),
        builder: (context,snapshot) { 
          if(ConnectionState.done == snapshot.connectionState){
            var per = json.decode(snapshot.data);
            return Container(
              height: getProportionateScreenHeight(120),
                      width: getProportionateScreenWidth(150),
              decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(50))),
              child: mostrarImagen(per['persona']['foto'], context),
            );
          }
          else{
            return CircleAvatar(
              radius: 55.0,
              backgroundImage:null,
                    //imagen != null ? NetworkImage(imagen) : AssetImage(imagenPrueba),
              backgroundColor: kPrimaryColor,
            );
          }
        },
      )
  );
}

_propuestasServicio({String text, GestureTapCallback onTap}) {
  return ListTile(
    title: Row(
      children: [
        Icon(
          Icons.receipt_long_sharp,
          color: kPrimaryColor,
        ),
        Padding(
          padding: EdgeInsets.only(left: 8.0),
          child: Text(text),
        )
      ],
    ),
    onTap: onTap,
  );
}

subirImagenVarias(File imagen,context,String title,
double radius,Function cancelar,Function enviar) {
    return showDialog(
        context: context,
        child: AlertDialog(
          title: Text(title),
          actions: [
            FlatButton(
              onPressed:cancelar, 
              child: Text('Cancelar')),
            FlatButton(
              onPressed: enviar, 
              child: Text('Enviar'))
          ],
          content: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: CircleAvatar(
              maxRadius: radius,
              backgroundImage: FileImage(imagen),
            ),
            height: getProportionateScreenHeight(200),
            width: getProportionateScreenWidth(150),
          ),
        ));
  }
TextStyle stiloText([double textSize]){
  return TextStyle(
    color: Colors.white,
    fontSize: textSize == null ? 17.0 : textSize,
    fontWeight: FontWeight.w300,
  ); 
}
TextStyle estiloText([double textSize]){
  return TextStyle(
    color: Colors.black,
    fontSize: textSize == null ? 17.0 : textSize,
    fontWeight: FontWeight.w300,
  ); 
}

ingresarCodigoOtp(context, codigo,Function callback) {
  return showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Ingrese el código enviado a tu correo' ,style: estiloText(),),
        elevation: 0.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            PinCodeTextField(
              showCursor: true,
              appContext: context,
              cursorColor: kSecondaryColor,
              animationType: AnimationType.fade,
              animationCurve: Curves.easeInBack,
              length: 6,
              obscureText: true,
              obscuringCharacter: '*',
              autoFocus: false,
              dialogConfig: DialogConfig(dialogTitle: 'OTP'),
              pastedTextStyle: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
              pinTheme: pintheme(),
              enableActiveFill: false,
              keyboardType: TextInputType.number,
              onCompleted: (v) {                
                  codigo = v;
              },
              onChanged: (value) {
                codigo = value;
              },
            ),
            DefaultButton(
              text: 'Validar',
              color: kSecondaryColor,
              press: () async {
                if(codigo.length == 6){
                  return callback(codigo);
                }
              },
            )
          ],
        ),
      );
    },
  );
}